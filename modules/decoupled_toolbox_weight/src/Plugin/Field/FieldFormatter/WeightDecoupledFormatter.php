<?php

namespace Drupal\decoupled_toolbox_weight\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\GenericDecoupledFormatter;
use Drupal\weight\Plugin\Field\FieldType\WeightItem;

/**
 * Plugin implementation of the 'decoupled_weight' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_weight",
 *   label = @Translation("Weight decoupled formatter"),
 *   field_types = {
 *     "weight",
 *   }
 * )
 */
class WeightDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    if (!$item instanceof WeightItem) {
      throw new InvalidContentException($this->t('Not a valid weight field item.'));
    }

    return (int) $item->getString();
  }

}
