<?php

namespace Drupal\decoupled_toolbox_comment\Plugin\Field\FieldFormatter;

use Drupal\comment\Plugin\Field\FieldType\CommentItem;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\GenericDecoupledFormatter;
use Drupal\decoupled_toolbox\Service\EntityViewDisplayManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'decoupled_comment_field' formatter.
 */
#[FieldFormatter(
  id: 'decoupled_comment_field',
  label: new TranslatableMarkup('Comment field decoupled formatter'),
  field_types: [
    'comment',
  ],
)]
class CommentFieldDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The comment storage.
   *
   * @var \Drupal\comment\CommentStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    ModuleHandlerInterface $module_handler,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $module_handler);
    $this->currentUser = $current_user;
    $this->storage = $entity_type_manager->getStorage('comment');
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $defautSettings = parent::defaultSettings();

    $defautSettings['view_mode'] = 'default';

    return $defautSettings;
  }

  /**
   * Provides a list of comment view modes for the configured comment type.
   *
   * @return array
   *   Associative array keyed by view mode key and having the view mode label
   *   as value.
   */
  protected function getViewModes() {
    return $this->entityDisplayRepository->getViewModeOptionsByBundle('comment', $this->getFieldSetting('comment_type'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    // Mostly taken from CommentDefaultFormatter.
    $form = parent::settingsForm($form, $form_state);
    $view_modes = $this->getViewModes();
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Comments view mode'),
      '#description' => $this->t('Select the view mode used to show the list of comments.'),
      '#default_value' => $this->getSetting('view_mode'),
      '#options' => $view_modes,
      // Only show the select element when there are more than one options.
      '#access' => count($view_modes) > 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $view_mode = $this->getSetting('view_mode');
    $view_modes = $this->getViewModes();
    $view_mode_label = $view_modes[$view_mode] ?? 'default';
    $summary[] = $this->t('Comment view mode: @mode', ['@mode' => $view_mode_label]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, $langCode): FieldValueAndOptions {
    $this->validateSettingsOnRender();

    // Disable the cache.
    // @todo This is a temporary workaround because contexts
    //   are not yet supported by decoupled_toolbox (#3484064).
    /** @var \Drupal\decoupled_toolbox\Service\DecoupledRenderer $renderer */
    $renderer = \Drupal::service('decoupled.renderer');
    $renderer->disableCache();

    // The next code lines are inspired by the CommentDefaultFormatter.
    if ($items->status === CommentItemInterface::HIDDEN
      || !empty($entity->in_preview)
      || in_array($this->viewMode, [
        'search_result',
        'search_index',
      ], TRUE)) {
      // Do not render this field.
      return $this->getNoValueReturn();
    }

    $comment_settings = $this->getFieldSettings();
    $prerenderedComments = [];

    $field_name = $this->fieldDefinition->getName();
    $entity = $items->getEntity();

    if ($this->currentUser->hasPermission('access comments ' . $this->getFieldSetting('comment_type')) || $this->currentUser->hasPermission('administer comments')) {
      if ($entity->get($field_name)->comment_count || $this->currentUser->hasPermission('administer comments')) {
        $mode = $comment_settings['default_mode'];
        $comments_per_page = $comment_settings['per_page'];

        /** @var \Drupal\comment\CommentInterface[] $comments */
        $comments = $this->storage->loadThread($entity, $field_name, $mode, $comments_per_page, $this->getSetting('pager_id'));

        if ($comments) {
          foreach ($comments as $comment) {
            // Prerender each comment entity.
            // @todo [#3484064] Due to #3484064, we do not use cache metadata
            //   yet. When contexts will be supported, we may return the
            //   cache metadata to the result.
            $commentCacheTags = [];
            $prerender = $renderer->renderByEntityTypeAndId(
              $comment->getEntityTypeId(),
              $comment->id(),
              $commentCacheTags,
              $this->getSetting('view_mode') ?? EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID
            );

            if (empty($prerender['render'])) {
              continue;
            }

            $prerenderedComments[] = $prerender['render'];
          }
        }
      }
    }

    // @todo [#3484064] Return the cache metadata when #3484064 will be fixed.
    return new FieldValueAndOptions($this->getDecoupledFieldKey(), $prerenderedComments, $this->getDecoupledFieldLocation());
  }

}
