# OpenAPI decoupled toolbox

Integration of decoupled toolbox with Open api module.
https://www.drupal.org/project/openapi
The documentation is generate for Open API 3.0 contrary to the parent module.

# Requirements

Open API module: https://www.drupal.org/project/openapi

# Recommended module

Open API ui: https://www.drupal.org/project/openapi_ui

### Library for open api

Swagger UI: https://www.drupal.org/project/openapi_ui_swagger
Or/and
Redoc UI: https://www.drupal.org/project/openapi_ui_redoc

# Installation

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

# Usage

## Add config entities
@todo
## Visit the endpoint/UI
@todo
