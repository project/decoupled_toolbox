<?php

namespace Drupal\openapi_decoupled_toolbox\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Render\RendererInterface;
use Drupal\openapi_decoupled_toolbox\OpenapiDecoupledToolboxTrait;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenApiDecoupledToolboxForm.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class OpenApiDecoupledToolboxForm extends EntityForm {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $requestStack;
  protected $entityBundleInfo;

  use OpenapiDecoupledToolboxTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, RequestStack $request_stack, EntityTypeBundleInfoInterface  $entity_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
    $this->entityBundleInfo = $entity_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('request_stack'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $definitions = $this->entityTypeManager->getDefinitions();
    $options = [];
    foreach ($definitions as $entity_type => $def) {
      $view_modes = $this->entityTypeManager->getStorage('entity_view_display')
        ->loadByProperties(['targetEntityType' => $entity_type]);
      if (empty($view_modes)) {
        continue;
      }
      $options[$entity_type] = $def->getLabel()->__toString();
    }
    asort($options);
    /** @var  $open_api_decoupled_toolbox \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox */
    $open_api_decoupled_toolbox = $this->entity;
    $size = 1;
    if (count($options) > 10) {
      $size = count($options);
      if ($size > 20) {
        $size = 20;
      }
    }

    $entity_type = $form_state->getValue('entity_type') ?? $open_api_decoupled_toolbox->getTargetEntityType();
    if (empty($entity_type)) {
      $entity_type = $this->requestStack->getCurrentRequest()->query->get('entity_type');
    }
    /*  if (empty($entity_type)) {
        $options = ['select' => t('- Select -')] + $options;
      }*/
    $form['container_entity_type'] = [
      '#type' => 'container',
      '#prefix' => '<div id="ajax-entity-type">',
      '#suffix' => '</div>',
    ];
    $form['container_entity_type']['entity_type'] = [
      '#type' => 'select',
      '#title' => t('Entity type'),
      '#required' => TRUE,
      '#default_value' => $entity_type,
      '#size' => $size,
      '#options' => $options,
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'ajaxEntityType'],
        'wrapper' => 'ajax-entity-type',
        'method' => 'replaceWith',
      ],
    ];
    $bundle = $form_state->getValue('bundle') ?? $open_api_decoupled_toolbox->getTargetBundle();
    if (empty($bundle)) {
      $bundle = [$this->requestStack->getCurrentRequest()->query->get('bundle')];
    }
    $form['container_entity_type']['bundle'] = [
      '#type' => 'hidden',
      '#default_value' => $bundle,
      '#title' => t('Bundle'),
      '#ajax' => [
        'callback' => [$this, 'ajaxEntityType'],
        'wrapper' => 'ajax-entity-type',
        'method' => 'replaceWith',
      ],
    ];
    $display = $form_state->getValue('display') ?? $open_api_decoupled_toolbox->getTargetDisplay();
    if (empty($display)) {
      $display = [$this->requestStack->getCurrentRequest()->query->get('display')];
    }
    $form['container_entity_type']['display'] = [
      '#type' => 'hidden',
      '#default_value' => $display,
      '#title' => t('Display'),
    ];
    /* You will need additional form elements for your custom properties. */

    $this->generateBundleForm($form, $form_state);
    $this->generateDisplayForm($form, $form_state);
    return $form;
  }

  /**
   * Generate bundle form options.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  private function generateBundleForm(array &$form, FormStateInterface $form_state): bool {

    $definitions = $this->entityTypeManager->getDefinitions();
    $triggering_element = $form_state->getTriggeringElement();
    // Trigger ajax or default value.
    if ((empty($triggering_element['#name']) || $triggering_element['#name'] !== 'entity_type') && empty($form['container_entity_type']['bundle']['#default_value'])) {
      return FALSE;
    }
    $entity_type = $form_state->getValue('entity_type') ?? $form['container_entity_type']['entity_type']['#default_value'];
    if (empty($definitions[$entity_type])) {
      return FALSE;
    }
    $bundle_info =  $this->entityBundleInfo->getBundleInfo($entity_type);
    $options_bundle = [];
    foreach ($bundle_info as $bundle_name => $bundle) {
      $options_bundle[$bundle_name] = $bundle['label'];
    }
    $form['container_entity_type']['bundle']['#type'] = 'checkboxes';
    $form['container_entity_type']['bundle']['#required'] = TRUE;
    $form['container_entity_type']['bundle']['#multiple'] = TRUE;
    $form['container_entity_type']['bundle']['#size'] = 10;
    $form['container_entity_type']['bundle']['#options'] = $options_bundle;
    return TRUE;
  }

  /**
   * Generate display form options.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function generateDisplayForm(array &$form, FormStateInterface $form_state): bool {
    $triggering_element = $form_state->getTriggeringElement();
    if (empty($form['container_entity_type']['display']['#default_value']) && (empty($triggering_element['#name']) || strpos($triggering_element['#name'], 'bundle[') !== 0)) {
      return FALSE;
    }
    $entity_type = $form_state->getValue('entity_type') ?? $form['container_entity_type']['entity_type']['#default_value'];
    $bundle = $form_state->getValue('bundle') ?? $form['container_entity_type']['bundle']['#default_value'];
    if (empty($entity_type) || empty($bundle)) {
      return FALSE;
    }
    /** @var  $view_modes \Drupal\field_layout\Entity\FieldLayoutEntityViewDisplay[] */
    $view_modes = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->loadByProperties([
        'targetEntityType' => $entity_type,
      ]);
    if (empty($view_modes)) {
      return FALSE;
    }
    // Remove display already set.
    $decoupled_config = $this->getDecoupledToolboxEnabledEntityTypes($entity_type, $bundle);

    $options_display = [];
    /** @var  $open_api_decoupled_toolbox \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox */
    $open_api_decoupled_toolbox = $this->entity;
    foreach ($view_modes as $display) {
      foreach ($decoupled_config as $config) {
        if ($config->id() == $open_api_decoupled_toolbox->id()) {
          continue;
        }
        if (in_array($display->getMode(), $config->getTargetDisplay(), TRUE)) {
          continue (2);
        }
      }
      $options_display[$display->getMode()] = $display->label() ?? $display->getMode();
    }

    $form['container_entity_type']['display']['#type'] = 'checkboxes';
    $form['container_entity_type']['display']['#required'] = TRUE;
    $form['container_entity_type']['display']['#multiple'] = TRUE;
    $form['container_entity_type']['display']['#size'] = 10;
    $form['container_entity_type']['display']['#options'] = $options_display;
    return TRUE;
  }

  /**
   * Ajax callback of node bundle.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public static function ajaxEntityType(array &$form, FormStateInterface $form_state) {
    return $form['container_entity_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var  $open_api_decoupled_toolbox \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox */
    $open_api_decoupled_toolbox = $this->entity;
    // Validate this pair entity type bundle and display is not already set.
    if (!empty($open_api_decoupled_toolbox->getTargetBundle()) && !empty($open_api_decoupled_toolbox->getTargetDisplay())) {
      // Get config entities filter by type, bundle and display.
      $config_decoupled_load = $this->getDecoupledToolboxEnabledEntityTypes($open_api_decoupled_toolbox->getTargetEntityType(), $open_api_decoupled_toolbox->getTargetBundle(), $open_api_decoupled_toolbox->getTargetDisplay());
      foreach ($config_decoupled_load as $config_decoupled) {
        if (empty($config_decoupled)) {
          continue;
        }
        // Not same entity.
        if ($config_decoupled->id() == $open_api_decoupled_toolbox->id()) {
          continue;
        }
        $url = Url::fromRoute('entity.openapi_decoupled_toolbox.edit_form', ['openapi_decoupled_toolbox' => $config_decoupled->id()]);
        $project_link = Link::fromTextAndUrl($config_decoupled->getConfigDependencyName(), $url);
        $project_link = $project_link->toRenderable();
        $form_state->setError($form, t('There is already one config entities with this entity type, bundle and display pair : @link', ['@link' => $this->renderer->render($project_link)]));
      }
    }

  }

  /**
   * Generate the id of decoupled toolbox entity.
   *
   * @param $id
   *
   * @return mixed|string
   */
  private function generateId($id) {
    if (strlen($id) > 255) {
      $id = substr($id, 0, 255);
    }
    $entity = $this->getDecoupledToolboxById($id);
    if (empty($entity)) {
      return $id;
    }
    return $this->generateId('bis_' . $id);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var  $open_api_decoupled_toolbox \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox */
    $open_api_decoupled_toolbox = $this->entity;
    if ($open_api_decoupled_toolbox->isNew()) {
      $id = $open_api_decoupled_toolbox->getTargetEntityType() . '_' . implode('_', $open_api_decoupled_toolbox->getTargetBundle()) . '_' . implode('_', $open_api_decoupled_toolbox->getTargetDisplay());
      $this->entity->set('id', $this->generateId($id));
      $this->entity->set('label', $open_api_decoupled_toolbox->getTargetEntityType() . ' ' . implode(' ', $open_api_decoupled_toolbox->getTargetBundle()));
    }
    $status = $open_api_decoupled_toolbox->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Open api decoupled toolbox.', [
            '%label' => $open_api_decoupled_toolbox->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Open api decoupled toolbox.', [
            '%label' => $open_api_decoupled_toolbox->label(),
          ]));
    }
    $form_state->setRedirectUrl($open_api_decoupled_toolbox->toUrl('collection'));
  }

}
