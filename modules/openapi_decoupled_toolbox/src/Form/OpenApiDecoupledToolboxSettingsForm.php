<?php

namespace Drupal\openapi_decoupled_toolbox\Form;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OpenApiDecoupledToolboxSettingsForm contains settings of module.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class OpenApiDecoupledToolboxSettingsForm extends ConfigFormBase {

  protected $renderer;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    parent:: __construct($config_factory);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openapi_decoupled_toolbox_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openapi_decoupled_toolbox.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('openapi_decoupled_toolbox.settings');
    $contact = $config->get('contact') ?? [];
    $url = Url::fromUri('https://swagger.io/specification/#contact-object', ['attributes' => ['target' => '_blank']]);
    $project_link = Link::fromTextAndUrl(t('Open API doc'), $url);
    $project_link = $project_link->toRenderable();
    $form['contact'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => TRUE,
      '#title' => t('Contact'),
      '#description' => $this->t('The open API contact information for the exposed API. @link', ['@link' => $this->renderer->render($project_link)]),
    ];
    $form['contact']['include'] = [
      '#type' => 'checkbox',
      '#title' => t('Include'),
      '#default_value' => $contact['include'] ?? FALSE,
      '#description' => $this->t('Check if you want to include the contact information inside open API'),
    ];
    $form['contact']['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $contact['name'] ?? '',
      '#description' => $this->t('The identifying name of the contact person/organization.'),
      '#states' => [
        'visible' => [
          'input[name="contact[include]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['contact']['url'] = [
      '#type' => 'url',
      '#title' => t('Url'),
      '#default_value' => $contact['url'] ?? '',
      '#description' => $this->t('The URL pointing to the contact information. MUST be in the format of a URL.'),
      '#states' => [
        'visible' => [
          'input[name="contact[include]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['contact']['email'] = [
      '#type' => 'email',
      '#title' => t('Email'),
      '#default_value' => $contact['email'] ?? '',
      '#description' => $this->t('The email address of the contact person/organization. MUST be in the format of an email address.'),
      '#states' => [
        'visible' => [
          'input[name="contact[include]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $url = Url::fromUri('https://swagger.io/specification/#license-object', ['attributes' => ['target' => '_blank']]);
    $project_link = Link::fromTextAndUrl(t('Open API doc'), $url);
    $project_link = $project_link->toRenderable();
    $license = $config->get('license') ?? [];
    $form['license'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => TRUE,
      '#title' => t('License'),
      '#description' => $this->t('The open API licence information for the exposed API. @link', ['@link' => $this->renderer->render($project_link)]),
    ];
    $form['license']['include'] = [
      '#type' => 'checkbox',
      '#title' => t('Include'),
      '#default_value' => $license['include'] ?? FALSE,
      '#description' => $this->t('Check if you want to include the license information inside open API'),

    ];
    $form['license']['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $license['name'] ?? '',
      '#description' => $this->t('The license name used for the API.'),
      '#states' => [
        'required' => [
          'input[name="license[include]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          'input[name="license[include]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['license']['url'] = [
      '#type' => 'url',
      '#title' => t('Url'),
      '#default_value' => $license['url'] ?? '',
      '#description' => $this->t('A URL to the license used for the API. MUST be in the format of a URL.'),
      '#states' => [
        'visible' => [
          'input[name="license[include]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $url = Url::fromUri('https://swagger.io/specification/#server-object', ['attributes' => ['target' => '_blank']]);
    $project_link = Link::fromTextAndUrl(t('Open API doc'), $url);
    $project_link = $project_link->toRenderable();
    // Container for our repeating fields.
    $description = $this->t('Manage all the environment server for open API decoupled toolbox.');
    $description .= $this->t('The current environment is automatically add. @link', ['@link' => $this->renderer->render($project_link)]);
    $form['servers'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => TRUE,
      '#title' => t('Servers'),
      '#description' => $description,
      '#prefix' => '<div id="ajax-server">',
      '#suffix' => '</div>',
    ];

    $triggering_element = $form_state->getTriggeringElement();


    $server = $config->get('servers') ?? [];
    $default_count = 0;
    if (!empty($server)) {
      uasort($server, [SortArray::class, 'sortByWeightElement']);
      $default_count = count($server);

    }
    $count_server = $form_state->get('count_server') ?? $default_count;
    // Trigger ajax or default value.
    if (!empty($triggering_element['#name']) && $triggering_element['#name'] === 'add_server') {

      $count_server++;
      $form_state->set('count_server', $count_server);
    }
    $delta_key_remove = -1;
    if (!empty($triggering_element['#name']) && strpos($triggering_element['#name'], 'remove_server_') === 0) {
      $triger_explode = explode('_', $triggering_element['#name']);
      $delta_key_remove = $triger_explode[2] ?? -1;
    }
    $form['servers']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Url'),
        $this->t('Description'),
        $this->t('Remove'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No servers added.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];
    // Add our names fields.
    for ($delta = 0; $delta < $count_server; $delta++) {
      if ($delta_key_remove == $delta) {
        continue;
      }
      $form['servers']['table'][$delta]['#attributes']['class'][] = 'draggable';
      $form['servers']['table'][$delta]['#weight'] = $server[$delta]['weight'];
      $form['servers']['table'][$delta]['url'] = [
        '#type' => 'url',
        '#title' => $this->t('Url'),
        '#default_value' => $server[$delta]['url'],
        '#required' => TRUE,
      ];
      $form['servers']['table'][$delta]['description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Description'),
        '#default_value' => $server[$delta]['description'],
      ];

      $form['servers']['table'][$delta]['remove'] = [
        '#type' => 'button',
        '#value' => $this->t('Remove'),
        '#name' => 'remove_server_' . $delta,
        '#default_value' => $server[$delta],
      ];
      $form['servers']['table'][$delta]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight'),
        // '#title_display' => 'invisible',
        '#default_value' => $server[$delta]['weight'] ?? 0,
        '#attributes' => [
          'class' => [
            'table-sort-weight',
          ],
        ],
      ];
    }
    if (!empty($triggering_element['#name']) && strpos($triggering_element['#name'], 'remove_server_') === 0) {
      $count_server--;
      if ($count_server < 0) {
        $count_server = 0;
      }
      $form_state->set('count_server', $count_server);
    }
    // Button to add more names.
    $form['servers']['add_server'] = [
      '#type' => 'button',
      '#name' => 'add_server',
      '#value' => $this->t('Add server'),
      '#ajax' => [
        'callback' => [$this, 'ajaxServer'],
        'wrapper' => 'ajax-server',
        'method' => 'replaceWith',
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Generate bundle form options.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  public static function ajaxServer(array &$form, FormStateInterface $form_state): bool {

    // $form_state->setRebuild();
    return $form['servers'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $servers = $form_state->getValue('servers');
    // Remove 'remove' button.
    foreach ($servers['table'] as &$row_server) {
      if (!empty($row_server['remove'])) {
        unset($row_server['remove']);
      }
    }
    // Retrieve the configuration.
    $this->configFactory->getEditable('openapi_decoupled_toolbox.settings')
      ->set('contact', $form_state->getValue('contact'))
      ->set('license', $form_state->getValue('license'))
      ->set('servers', $servers['table'] ?? [])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
