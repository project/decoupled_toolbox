<?php

namespace Drupal\openapi_decoupled_toolbox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Open api decoupled toolbox entity.
 *
 * @ConfigEntityType(
 *   id = "openapi_decoupled_toolbox",
 *   label = @Translation("Open api decoupled toolbox"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\openapi_decoupled_toolbox\OpenApiDecoupledToolboxListBuilder",
 *     "form" = {
 *       "add" =
 *   "Drupal\openapi_decoupled_toolbox\Form\OpenApiDecoupledToolboxForm",
 *       "edit" =
 *   "Drupal\openapi_decoupled_toolbox\Form\OpenApiDecoupledToolboxForm",
 *       "delete" =
 *   "Drupal\openapi_decoupled_toolbox\Form\OpenApiDecoupledToolboxDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\openapi_decoupled_toolbox\OpenApiDecoupledToolboxHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "oa_dt",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "entity_type" = "entity type",
 *     "bundle" = "bundle",
 *     "display" = "display",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_type",
 *     "bundle",
 *     "display",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/config/services/openapi/decoupled-toolbox/{openapi_decoupled_toolbox}",
 *     "add-form" = "/admin/config/services/openapi/decoupled-toolbox/add",
 *     "edit-form" =
 *   "/admin/config/services/openapi/decoupled-toolbox/{openapi_decoupled_toolbox}/edit",
 *     "delete-form" =
 *   "/admin/config/services/openapi/decoupled-toolbox/{openapi_decoupled_toolbox}/delete",
 *     "collection" = "/admin/config/services/openapi/decoupled-toolbox/collection"
 *   }
 * )
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class OpenApiDecoupledToolbox extends ConfigEntityBase implements OpenApiDecoupledToolboxInterface {

  /**
   * The Open api decoupled toolbox ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Open api decoupled toolbox label.
   *
   * @var string
   */
  protected $label;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * The target bundles.
   *
   * @var array
   */
  protected $bundle;

  /**
   * The target displays.
   *
   * @var array
   */
  protected $display;

  /**
   * Return target entity type.
   *
   * @return string
   *   The target entity type.
   */
  public function getTargetEntityType(): string {
    return $this->entity_type ?? '';
  }

  /**
   * Return target bundles.
   *
   * @return array
   *   The target bundles.
   */
  public function getTargetBundle(): array {
    return $this->bundle ? array_filter($this->bundle) : [];
  }

  /**
   * Return target displays.
   *
   * @return array
   *   The target displays.
   */
  public function getTargetDisplay(): array {
    return $this->display ? array_filter($this->display) : [];
  }

}
