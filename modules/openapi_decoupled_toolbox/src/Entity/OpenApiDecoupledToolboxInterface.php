<?php

namespace Drupal\openapi_decoupled_toolbox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Open api decoupled toolbox entities.
 */
interface OpenApiDecoupledToolboxInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
