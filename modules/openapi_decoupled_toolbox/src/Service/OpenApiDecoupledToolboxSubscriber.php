<?php

namespace Drupal\openapi_decoupled_toolbox\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\Service\RequestEntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Standard field location solver.
 */
class OpenApiDecoupledToolboxSubscriber implements EventSubscriberInterface {

  use LoggerChannelTrait;

  public const OPEN__API_DECOUPLED__TOOLBOX__ID_KEY = 'decoupled_toolbox_key';

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RequestEntityInterface::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT] = ['onRenderedOutputBuilt'];
    return $events;
  }

  /**
   * Reacts on rendered output built.
   *
   * @param \Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent $event
   *   Event object.
   */
  public function onRenderedOutputBuilt(OnRenderedOutputBuiltEvent $event): void {
    $this->setDecoupledKey($event->getRenderedOutput(), $event->getEntity(), $event->getDisplay());
  }

  /**
   * Inject decoupled key inside output.
   *
   * @param array $outputSource
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $display
   *
   * @return void
   */
  public function setDecoupledKey(array &$outputSource, EntityInterface $entity, $display): void {
    // Insert unique identifier base on type, bundle and display.
    // Usefull for open API discriminator.
    $outputSource[$this::OPEN__API_DECOUPLED__TOOLBOX__ID_KEY] = $entity->getEntityTypeId() . '.' . $entity->bundle() . '.' . $display;
  }

}
