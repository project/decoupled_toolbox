<?php

namespace Drupal\openapi_decoupled_toolbox;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Open api decoupled toolbox entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class OpenApiDecoupledToolboxHtmlRouteProvider extends AdminHtmlRouteProvider {

}
