<?php

namespace Drupal\openapi_decoupled_toolbox;

use Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox;
use Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolboxInterface;
use Drupal\rest\RestResourceConfigInterface;

/**
 * Common functions for inspecting decoupled toolbox resources.
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
trait OpenapiDecoupledToolboxTrait {

  /**
   * @param $id
   *
   * @return \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolboxInterface|null
   */
  protected function getDecoupledToolboxById($id): ?OpenApiDecoupledToolboxInterface {
    return $this->entityTypeManager->getStorage('openapi_decoupled_toolbox')
      ->load($id);
  }

  /**
   * Gets entity types that are enabled for decoupled toolbox.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $bundle
   *   Filter by the entity bundle if send.
   * @param array $display
   *   Filter by the display if send.
   *
   * @return \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox[]
   *   Entity types that are enabled.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDecoupledToolboxEnabledEntityTypes($entity_type_id = NULL, array $bundle = [], array $display = []): array {
    if (!empty($entity_type_id)) {
      // Can't filter by array bundle or display,
      // @see https://www.drupal.org/project/drupal/issues/2248567.
      /** @var  $entities OpenApiDecoupledToolbox[] */
      $entities = $this->entityTypeManager->getStorage('openapi_decoupled_toolbox')
        ->loadByProperties(['entity_type' => $entity_type_id]);
      if (empty($bundle)) {
        return $entities;
      }
      foreach ($entities as $id => $config_decoupled) {
        // Don't find bundle.
        if (count(array_diff($bundle, $config_decoupled->getTargetBundle())) == count($bundle)) {
          unset($entities[$id]);
        }
        // Don't find display.
        if (!empty($display) && count(array_diff($display, $config_decoupled->getTargetDisplay())) == count($display)) {
          unset($entities[$id]);
        }
      }
      return $entities;
    }
    return $this->entityTypeManager->getStorage('openapi_decoupled_toolbox')
      ->loadMultiple();
  }

  /**
   * Gets entity types that are enabled for rest.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   Entity types that are enabled.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRestEnabledEntityTypes($entity_type_id = NULL): array {
    $entity_types = [];
    $resource_config = $this->getResourceConfigs();

    if (empty($resource_config)) {
      return $entity_types;
    }
    if ($entity_type = $this->getEntityType($resource_config)) {
      if (!$entity_type_id || $entity_type->id() == $entity_type_id) {
        $entity_types[$entity_type->id()] = $entity_type;
      }
    }

    return $entity_types;
  }

  /**
   * Gets the REST config resources.
   *
   * @param array $options
   *   The options to generate the output.
   *
   * @return \Drupal\rest\RestResourceConfigInterface|false
   *   The REST config resources.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getResourceConfigs(array $options = []) {

    /** @var  $resource_config \Drupal\rest\Entity\RestResourceConfig */
    $resource_config = $this->entityTypeManager->getStorage('rest_resource_config')
      ->load('deoupled_toolbox_collection');
    if (empty($resource_config)) {
      return FALSE;
    }
    if ($resource_config->status() == FALSE) {
      return FALSE;
    }
    return $resource_config;
  }

  /**
   * Gets the entity type if any for REST resource.
   *
   * @param \Drupal\rest\RestResourceConfigInterface $resource_config
   *   The REST config resource.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   The Entity Type or null.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityType(RestResourceConfigInterface $resource_config) {
    $resource_plugin = $resource_config->getResourcePlugin();
    return $this->entityTypeManager->getDefinition($resource_plugin->getPluginDefinition()['entity_type']);
  }

}
