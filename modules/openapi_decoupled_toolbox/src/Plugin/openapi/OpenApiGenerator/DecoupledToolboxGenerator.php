<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator;

use Symfony\Component\Routing\Route;

/**
 * Generate openAPI plugin for the decoupled toolbox controller.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class DecoupledToolboxGenerator extends DecoupledToolboxGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function getPathsResponses(array $definitions) {
    $route = $this->routingProvider->getRouteByName('decoupled_toolbox.entity_decoupled_data.collection');

    $bundle_name = isset($this->getOptions()['bundle_name']) ? $this->getOptions()['bundle_name'] : NULL;
    $entity_type_id = isset($this->getOptions()['entity_type_id']) ? $this->getOptions()['entity_type_id'] : NULL;
    $config_decoupleds = $this->getDecoupledToolboxEnabledEntityTypes();

    foreach ($route->getMethods() as $method) {
      foreach ($config_decoupleds as $config_decoupled) {
        if (!empty($entity_type_id) && $config_decoupled->getTargetEntityType() != $entity_type_id) {
          continue;
        }
        $path = $route->getPath();
        $path = str_replace('{type}', $config_decoupled->getTargetEntityType(), $path);
        foreach ($config_decoupled->getTargetBundle() as $bundle) {
          if (!empty($bundle_name) && $bundle != $bundle_name) {
            continue;
          }
          $open_api_method = strtolower($method);
          $path = str_replace('{bundle}', $bundle, $path);
          $formats = $this->getMethodSupportedFormats($route);
          $path_method_spec = $this->generatePathMethod($config_decoupled, $bundle, $method, $definitions, $formats);
          // $path_method_spec['schemes'] = [$this->request->getScheme()];

          $path_method_spec['security'] = $this->getRouteSecurity($route);
          $api_paths[$path][$open_api_method] = $path_method_spec;

        }
      }
    }
    return $api_paths;
  }

  /**
   * Get OpenAPI parameters for a route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route.
   *
   * @return array
   *   The resource parameters.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRouteParameters(Route $route): array {
    $config_decoupleds = $this->getDecoupledToolboxEnabledEntityTypes();
    $enum = ['bundle' => [], 'type' => []];
    foreach ($config_decoupleds as $config_decoupled) {
      $enum['bundle'] = array_merge($enum['bundle'], $config_decoupled->getTargetBundle());
      $enum['type'] = array_merge($enum['type'], [$config_decoupled->getTargetEntityType()]);
    }
    $parameters = [];
    $vars = $route->compile()->getPathVariables();
    foreach ($vars as $var) {
      $schema = ['type' => 'string'];
      if (!empty($enum[$var])) {
        $schema['enum'] = array_values($enum[$var]);
      }
      $schema['enum'] = array_unique($schema['enum']);
      $parameters[] = [
        'name' => $var,
        'schema' => $schema,
        'in' => 'path',
        'required' => TRUE,
      ];

    }
    return $parameters;
  }

  /**
   * Return the formats supported by the route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *
   * @return array|string[]
   */
  protected function getMethodSupportedFormats(Route $route): array {
    if ($formats = $route->getRequirement('_format')) {
      return explode('|', $formats);
    }

    return [];
  }

  /**
   * Return auth security supported by the route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *
   * @return array
   */
  protected function getRouteSecurity(Route $route): array {
    //@todo _auth
    if ($auths = $route->getOption('_auth')) {
      foreach ($auths as $auth) {
        $security[] = [$auth => []];
      }
      return $security;
    }
    return [];
  }

  /**
   * Generate list of MIME Types based on a list of serializer formats.
   *
   * @param array $formats
   *   List of formats.
   *
   * @return array
   *   List of MIME Types based on $formats.
   *   The list is MIME Types are on the same order as the inserted $format
   */
  protected function generateMimeTypesFromFormats(array $formats): array {
    $mime_types = [];
    foreach ($formats as $format) {
      $mime_types[] = 'application/' . str_replace("_", '+', strtolower(trim($format)));
    }
    return $mime_types;
  }

  /**
   * Returns a list of supported Format on Decoupled toobox.
   *
   * @return array
   *   The list of supported formats.
   */
  protected function getDecoupledSupportedFormats(): array {
    return ['json'];
  }

}
