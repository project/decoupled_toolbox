<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator;

use Drupal\rest\RestResourceConfigInterface;
use Symfony\Component\Routing\Route;
use Exception;

/**
 * Generate openAPI plugin for the decoupled toolbox plugin REST.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class DecoupledToolboxRestGenerator extends DecoupledToolboxGeneratorBase {

  /**
   * Returns a list of supported Format on REST.
   *
   * @return array
   *   The list of supported formats.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRestSupportedFormats(): array {
    static $supported_formats = [];
    if (empty($supported_formats)) {
      $resource_config = $this->getResourceConfigs($this->getOptions());
      if (empty($resource_config)) {
        return [];
      }
      foreach ($resource_config->getMethods() as $method) {
        $formats = $this->getMethodSupportedFormats($method, $resource_config);
        $supported_formats = array_unique(array_merge($supported_formats, $formats), SORT_REGULAR);
      }
    }

    return $supported_formats;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumes(): array {
    return $this->generateMimeTypesFromFormats($this->getRestSupportedFormats());
  }

  /**
   * {@inheritdoc}
   */
  public function getProduces(): array {
    return $this->generateMimeTypesFromFormats($this->getRestSupportedFormats());
  }

  /**
   * {@inheritdoc}
   */
  public function getPathsResponses(array $definitions) {

    $bundle_name = isset($this->getOptions()['bundle_name']) ? $this->getOptions()['bundle_name'] : NULL;
    $entity_type_id = isset($this->getOptions()['entity_type_id']) ? $this->getOptions()['entity_type_id'] : NULL;
    $resource_config = $this->getResourceConfigs($this->getOptions());
    $config_decoupleds = $this->getDecoupledToolboxEnabledEntityTypes();
    if (!$resource_config) {
      return [];
    }
    $api_paths = [];
    /** @var \Drupal\rest\Plugin\ResourceBase $plugin
     * $resource_plugin = $resource_config->getResourcePlugin();*/
    foreach ($resource_config->getMethods() as $method) {
      if (!$route = $this->getRouteForResourceMethod($resource_config, $method)) {
        continue;
      }
      $open_api_method = strtolower($method);
      $formats = $this->getMethodSupportedFormats($method, $resource_config);
      foreach ($config_decoupleds as $config_decoupled) {
        if (!empty($entity_type_id) && $config_decoupled->getTargetEntityType() != $entity_type_id) {
          continue;
        }
        foreach ($config_decoupled->getTargetBundle() as $bundle) {
          $path = $route->getPath();
          // We prefer to generate path with type and bundle so we can describe
          // and separate response schema.
          $path = str_replace('{type}', $config_decoupled->getTargetEntityType(), $path);
          if (!empty($bundle_name) && $bundle !== $bundle_name) {
            continue;
          }
          $path = str_replace('{bundle}', $bundle, $path);
          $path_method_spec = $this->generatePathMethod($config_decoupled, $bundle, $method, $definitions, $formats);
          $path_method_spec['security'] = $this->getResourceSecurity($resource_config, $method, $formats);
          $api_paths[$path][$open_api_method] = $path_method_spec;
        }
      }
    }

    return $api_paths;
  }

  /**
   * Gets the matching for route for the resource and method.
   *
   * @param \Drupal\rest\RestResourceConfigInterface $resource_config
   *   The REST config resource.
   * @param string $method
   *   The HTTP method.
   *
   * @return \Symfony\Component\Routing\Route
   *   The route.
   *
   * @throws \Exception
   *   If no route is found.
   */
  protected function getRouteForResourceMethod(RestResourceConfigInterface $resource_config, $method): Route {

    $resource_plugin = $resource_config->getResourcePlugin();
    foreach ($resource_plugin->routes() as $route) {
      $methods = $route->getMethods();
      if (in_array($method, $methods, TRUE) !== FALSE) {
        return $route;
      }
    }

    throw new Exception("No route found for REST resource, {$resource_config->id()}, for method $method");
  }

  /**
   * Get the security information for the a resource.
   *
   * @param \Drupal\rest\RestResourceConfigInterface $resource_config
   *   The REST resource.
   * @param string $method
   *   The HTTP method.
   * @param string[] $formats
   *   The formats.
   *
   * @return array
   *   The security elements.
   *
   * @see http://swagger.io/specification/#securityDefinitionsObject
   */
  public function getResourceSecurity(RestResourceConfigInterface $resource_config, $method, array $formats): array {
    $security = [];
    foreach ($resource_config->getAuthenticationProviders($method) as $auth) {
      switch ($auth) {
        case 'basic_auth':
        case 'cookie':
        case 'oauth':
        case 'oauth2':
          // @TODO: #2977109 - Calculate oauth scopes required.
          $security[] = [$auth => []];
          break;
      }
    }

    // @todo Handle tokens that need to be set in headers.

    $route_name = 'rest.' . $resource_config->id() . ".$method";

    $routes = $this->routingProvider->getRoutesByNames([$route_name]);
    if (empty($routes) && count($formats) > 1) {
      $route_name .= ".{$formats[0]}";
      $routes = $this->routingProvider->getRoutesByNames([$route_name]);
    }
    if ($routes) {
      $route = array_pop($routes);
      // Check to see if route is protected by access checks in header.
      if ($route->getRequirement('_csrf_request_header_token')) {
        $security[] = ['csrf_token' => []];
      }
    }
    return $security;
  }

  /**
   * List of supported Serializer Formats for HTTP Method on a Rest Resource.
   *
   * @param string $method
   *   The HTTP method for generating the MIME Types. Example: GET, POST, etc.
   * @param \Drupal\rest\RestResourceConfigInterface $resource_config
   *   The resource configuration.
   *
   * @return array
   *   The list of MIME Types
   */
  protected function getMethodSupportedFormats($method, RestResourceConfigInterface $resource_config): array {
    if (empty($method)) {
      return [];
    }
    // The route ID.
    $route_id = "rest.{$resource_config->id()}.$method";

    // First Check the supported formats on the route level.
    /** @var \Symfony\Component\Routing\Route[] $route */
    $routes = $this->routingProvider->getRoutesByNames([$route_id]);
    if (!empty($routes) && array_key_exists($route_id, $routes) && $formats = $routes[$route_id]->getRequirement('_format')) {
      return explode('|', $formats);
    }

    // If no route level format was found, lets use
    // the RestResourceConfig formats for the given HTTP method.
    return $resource_config->getFormats($method);
  }

  /**
   * Generate list of MIME Types based on a list of serializer formats.
   *
   * @param array $formats
   *   List of formats.
   *
   * @return array
   *   List of MIME Types based on $formats.
   *   The list is MIME Types are on the same order as the inserted $format
   */
  protected function generateMimeTypesFromFormats(array $formats): array {
    $mime_types = [];
    foreach ($formats as $format) {
      $mime_types[] = 'application/' . str_replace("_", '+', strtolower(trim($format)));
    }
    return $mime_types;
  }

}
