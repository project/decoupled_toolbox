<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator;

use Drupal\Core\Authentication\AuthenticationCollectorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\DecoupledFormatterBase;
use Drupal\openapi\Plugin\openapi\OpenApiGeneratorBase;
use Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox;
use Drupal\openapi_decoupled_toolbox\OpenapiDecoupledToolboxTrait;
use Drupal\openapi_decoupled_toolbox\Service\OpenApiDecoupledToolboxSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Defines an OpenApi Schema Generator for the Decoupled toolbox module.
 *
 * @OpenApiGenerator(
 *   id = "decoupled_toolbox",
 *   label = @Translation("Decoupled toolbox"),
 *   deriver =
 *   "Drupal\openapi_decoupled_toolbox\Plugin\Derivative\OpenAPiDecoupledToolboxDeriver"
 * )
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
abstract class DecoupledToolboxGeneratorBase extends OpenApiGeneratorBase implements DecoupledToolboxGeneratorInterface {

  /**
   *
   */
  use OpenapiDecoupledToolboxTrait;

  /**
   * The current user session object.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * OpenApiGeneratorBase constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Unique plugin id.
   * @param array|mixed $plugin_definition
   *   Plugin instance definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routing_provider
   *   The routing provider.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The field manager.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\Core\Authentication\AuthenticationCollectorInterface $authentication_collector
   *   The authentication collector.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteProviderInterface $routing_provider, EntityFieldManagerInterface $field_manager, SerializerInterface $serializer, RequestStack $request_stack, ConfigFactoryInterface $config_factory, AuthenticationCollectorInterface $authentication_collector, Session $session) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $routing_provider, $field_manager, $serializer, $request_stack, $config_factory, $authentication_collector);
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('router.route_provider'),
      $container->get('entity_field.manager'),
      $container->get('serializer'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('authentication_collector'),
      $container->get('session')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSpecification(): array {
    $definitions = $this->getDefinitions();
    $spec = [
      'openapi' => '3.0.0',
      'info' => $this->getInfo(),
      'servers' => $this->getServers(),
      'security' => [$this->getSecurity()],
      'components' => [
        'schemas' => $definitions,
        'securitySchemes' => $this->getSecurityDefinitions(),
      ],
      'paths' => $this->getPathsResponses($definitions),
    ];

    // Strip any empty arrays which aren't required.
    $required = ['openapi', 'info', 'paths'];
    foreach ($spec as $key => $item) {
      if (is_array($item) && !in_array($key, $required) && !count($item)) {
        unset($spec[$key]);
      }
    }
    return $spec;
  }

  /**
   * Creates the 'info' portion of the API.
   *
   * @return array
   *   The info elements.
   */
  protected function getInfo(): array {
    $site_name = $this->configFactory->get('system.site')->get('name');
    $info = [
      'description' => $this->getApiDescription(),
      'title' => $site_name . ' - ' . $this->getApiName(),
      'version' => $this->configFactory->get('decoupled_toolbox.settings')
        ->get('version') ? (string) round($this->configFactory->get('decoupled_toolbox.settings')
        ->get('version'), 1) : '1.0',
    ];
    $this->getInfoProperties($info, 'contact');
    $this->getInfoProperties($info, 'license');
    return $info;
  }

  /**
   * Return url of different environment server.
   *
   * @return array
   */
  protected function getServers(): array {
    $server[] = [
      'url' => $this->request->getSchemeAndHttpHost(),
      'description' => t('Current environment.'),
    ];

    $servers_settings = $this->configFactory->get('openapi_decoupled_toolbox.settings')
      ->get('servers');
    $url_unique = [];
    foreach ($servers_settings as $server_setting) {
      if (empty($server_setting['url'])) {
        continue;
      }
      // Remove duplicate url, in case of current environment is same as the
      // settings.
      if (in_array($server_setting['url'], $url_unique, TRUE)) {
        continue;
      }
      $url_unique[] = $server_setting['url'];
      $tmp['url'] = $server_setting['url'];
      if (!empty($server_setting['description'])) {
        $tmp['description'] = $server_setting['description'];
      }
      $server[] = $tmp;
    }
    return $server;
  }

  /**
   * Get some information properties (set inside settings).
   *
   * @param array $info
   * @param $key_settings
   *
   * @return void|null
   */
  protected function getInfoProperties(array &$info, $key_settings) {

    $contact_settings = $this->configFactory->get('openapi_decoupled_toolbox.settings')
      ->get($key_settings);
    if (empty($contact_settings['include'])) {
      return NULL;
    }
    foreach ($contact_settings as $key_contact => $contact_value) {
      if ($key_contact === 'include' || empty($contact_value)) {
        continue;
      }
      $info[$key_settings][$key_contact] = $contact_value;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getJsonSchema($described_format, $entity_type_id, $bundle_name = NULL) {
    // No need to implements for open API 3.0.
  }

  /**
   * {@inheritdoc}
   */
  public function getSecurityDefinitions(): array {
    $base_url = $this->request->getSchemeAndHttpHost() . '/' . $this->request->getBasePath();
    $auth_providers = $this->authenticationCollector->getSortedProviders();
    $security_definitions = [];
    foreach ($auth_providers as $provider => $info) {
      $def = NULL;
      switch ($provider) {
        case 'cookie':
          $def = [
            'type' => 'apiKey',
            'name' => $this->session->getName(),
            'in' => 'cookie',
          ];
          break;

        case 'basic_auth':
          $def = [
            'type' => 'http',
            'scheme' => 'basic',
          ];
          break;

        case 'oauth2':
          $def = [
            'type' => 'oauth2',
            'description' => 'For more information see https://developers.getbase.com/docs/rest/articles/oauth2/requests',
            'flows' => [
              'password' => [
                'tokenUrl' => $base_url . 'oauth/token',
                'refreshUrl' => $base_url . 'oauth/token',
              ],
              'authorizationCode' => [
                'authorizationUrl' => $base_url . 'oauth/authorize',
                'tokenUrl' => $base_url . 'oauth/token',
                'refreshUrl' => $base_url . 'oauth/token',
              ],
              'implicit' => [
                'authorizationUrl' => $base_url . 'oauth/authorize',
                'refreshUrl' => $base_url . 'oauth/token',
              ],
              'clientCredentials' => [
                'tokenUrl' => $base_url . 'oauth/token',
                'refreshUrl' => $base_url . 'oauth/token',
              ],
            ],
          ];
          break;

        default:
          continue 2;
      }
      if ($def !== NULL) {
        $security_definitions[$provider] = $def;
      }
    }

    // Core's CSRF token doesn't have an auth provider.
    $security_definitions['csrf_token'] = [
      'type' => 'apiKey',
      'name' => 'X-CSRF-Token',
      'in' => 'header',
      'x-tokenUrl' => $base_url . 'user/token',
    ];

    return $security_definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions(): array {
    $bundle_name = isset($this->getOptions()['bundle_name']) ? $this->getOptions()['bundle_name'] : NULL;
    $entity_type_id = isset($this->getOptions()['entity_type_id']) ? $this->getOptions()['entity_type_id'] : NULL;
    static $definitions = [];
    if (!empty($definitions)) {
      return $definitions;
    }
    $entity_types = $this->getDecoupledToolboxEnabledEntityTypes($entity_type_id);
    foreach ($entity_types as $config_decoupled) {
      $this->buildDefintion($config_decoupled, $definitions, $bundle_name);
    }
    return $definitions;
  }

  /**
   *
   * @param \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox $config_decoupled
   *   The config open api decoupled entity.
   * @param array $definitions
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  /**
   * Build the schema open API definition.
   *
   * @param \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox $config_decoupled
   *   The config open api decoupled entity.
   * @param array $definitions
   *   The open API array schema definitions.
   * @param string $bundle_name
   *   The bundle options name.
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildDefintion(OpenApiDecoupledToolbox $config_decoupled, array &$definitions, $bundle_name = NULL): void {
    foreach ($config_decoupled->getTargetBundle() as $bundle) {
      // Filter by bundle option.
      if (!empty($bundle_name) && $bundle !== $bundle_name) {
        continue;
      }
      foreach ($config_decoupled->getTargetDisplay() as $display) {
        if (empty($display)) {
          continue;
        }
        $this->buildDefinitionEntityViewMode($config_decoupled->getTargetEntityType(), $bundle, $display, $definitions);
        if (empty($definitions[$config_decoupled->getTargetEntityType() . '.' . $bundle . '.' . $display])) {
          continue;
        }
        $definitions[$config_decoupled->getTargetEntityType() . '.' . $bundle . '.' . $display]['properties'][OpenApiDecoupledToolboxSubscriber::OPEN__API_DECOUPLED__TOOLBOX__ID_KEY]['type'] = 'string';
      }
    }
  }

  /**
   * Build definition entity view mode.
   *
   * @param string $entity_type
   *   The entity type of entity to generate definition.
   * @param string $bundle
   *   The bundle of entity to generate definition.
   * @param string $display
   *   The display of entity to generate definition.
   * @param array $definitions
   *   The open API array schema definitions.
   * @param array $parent
   *   An array of parent entities if it's entity reference.
   *
   * @return bool
   *   FALSE if can't generate.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildDefinitionEntityViewMode($entity_type, $bundle, $display, array &$definitions, array $parent = []): bool {
    /** @var  $view_mode \Drupal\Core\Entity\Entity\EntityViewDisplay */
    $view_mode = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load($entity_type . '.' . $bundle . '.' . $display);
    if (empty($view_mode)) {
      return FALSE;
    }
    $definitions[$entity_type . '.' . $bundle . '.' . $display] = [
      'type' => 'object',
      'title' => t('@type @bundle @display schema', [
        '@type' => $entity_type,
        '@bundle' => $bundle,
        '@display' => $display,
      ]),
      'description' => t("Describes the payload for '@type' '@bundle' entities with display: @display.", [
        '@type' => $entity_type,
        '@bundle' => $bundle,
        '@display' => $display,
      ]),
    ];
    $this->buildProperties($entity_type, $definitions, $view_mode, $bundle, $display, $parent);
    return TRUE;
  }

  /**
   * Build the properties.
   *
   * @param string $entity_type
   *   The entity type of entity to generate definition.
   * @param array $definitions
   *   The open API array schema definitions.
   * @param \Drupal\field_layout\Entity\FieldLayoutEntityViewDisplay $view_mode
   *   The entity view mode of entity to generate definition.
   * @param string $bundle
   *   The bundle of entity to generate definition.
   * @param string $display
   *   The display of entity to generate definition.
   * @param array $parent
   *   An array of parent entities if it's entity reference.
   *
   * @return false|void
   *   False if can't generate.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildProperties($entity_type, array &$definitions, EntityViewDisplay $view_mode, $bundle, $display, array $parent = []) {
    if (empty($view_mode->get('content'))) {
      return FALSE;
    }
    $def_key = $entity_type . '.' . $bundle . '.' . $display;
    $field_defintiions = $view_mode->get('fieldDefinitions');
    // Loop on each field display in view mode.
    foreach ($view_mode->get('content') as $field_name => $content) {
      if (empty($content["settings"]["decoupled_field_key"])) {
        continue;
      }
      // Set the first parent.
      if (empty($parent)) {
        $parent = [
          'type' => $entity_type,
          'bundle' => $bundle,
          'display' => $display,
        ];
      }

      // Special case for entity references.
      $build_ref = $this->buildEntityReferenceProperties($content, $field_name, $field_defintiions, $definitions, $parent);

      // No re-location option.
      if (empty($content["settings"]["decoupled_field_location"])) {
        if ($build_ref) {
          $possibilities = $this->getReferencePossibility($content, $field_name, $field_defintiions);
          // Only one bundle type.
          if (count($possibilities) == 1) {
            $possibility = reset($possibilities);
            $definitions[$def_key]['properties'][$content["settings"]["decoupled_field_key"]] = [
              'type' => 'array',
              'items' => ['$ref' => '#/components/schemas/' . $possibility['type'] . '.' . $possibility['bundle'] . '.' . $possibility['display'],],
            ];
            continue;
          }
          $definitions[$def_key]['properties'][$content["settings"]["decoupled_field_key"]] = [
            'type' => 'array',
          ];
          foreach ($possibilities as $possibility) {
            $definitions[$def_key]['properties'][$content["settings"]["decoupled_field_key"]]['items']['oneOf']['$ref'] = '#/components/schemas/' . $possibility['type'] . '.' . $possibility['bundle'] . '.' . $possibility['display'];
          }
        }
        $definitions[$def_key]['properties'][$content["settings"]["decoupled_field_key"]] = [];
        $this->buildItemField($content, $field_name, $definitions[$def_key]['properties'][$content["settings"]["decoupled_field_key"]], $content["settings"]["decoupled_field_key"]);
        continue;
      }
      $location = explode('/', $content["settings"]["decoupled_field_location"]);
      // Absolute path.
      if (empty($location[0]) && !empty($parent)) {
        $def_key = $parent['type'] . '.' . $parent['bundle'] . '.' . $parent['display'];
      }
      $location = array_filter($location);
      if (empty($location)) {
        continue;
      }
      $loc_key = current($location);
      if (!empty($definitions[$def_key]['properties'][$loc_key])) {
        $temp_proper = $this->buildLocationProperties($content, $field_name, $location, $content["settings"]["decoupled_field_key"]);

        $definitions[$def_key]['properties'][$loc_key]['properties'] = array_merge($definitions[$def_key]['properties'][$loc_key]['properties'], $temp_proper['properties']);

        continue;
      }
      $definitions[$def_key]['properties'][current($location)] = $this->buildLocationProperties($content, $field_name, $location, $content["settings"]["decoupled_field_key"]);
    }
  }

  /**
   * Build the parameter and responses endpoint.
   *
   * @see https://swagger.io/specification/#responses-object.
   *
   * @param \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox $config_decoupled
   *   The config open api decoupled entity.
   * @param string $bundle
   *   The bundle of entity to generate definition.
   * @param string $method
   *   The  method of route to generate definition.
   * @param array $definitions
   *   The open API array schema definitions.
   * @param array $formats
   *   The array of format possibilities.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function generatePathMethod(OpenApiDecoupledToolbox $config_decoupled, $bundle, $method, array $definitions, array $formats): array {
    $path_method_spec = [];
    $path_method_spec['parameters'] = [];
    // Add format parameter.
    if (!empty($formats)) {
      $format_parameter = [
        'name' => '_format',
        'in' => 'query',
        'schema' => [
          'type' => 'string',
          'enum' => $formats,
        ],
        'required' => TRUE,
        'description' => 'Request format',
      ];
      if (count($formats) == 1) {
        $format_parameter['schema']['default'] = $formats[0];
      }
      $path_method_spec['parameters'][] = $format_parameter;
    }

    // Add responses error.
    $path_method_spec['responses'] = $this->getErrorResponses();

    $schema_response = $this->getRefSchemaEntityType($definitions, $config_decoupled->getTargetEntityType(), $bundle);
    $number_responses = count($schema_response);

    // Only one schema response.
    if ($number_responses == 1) {
      $schema_response = array_pop($schema_response);
    }
    else {
      $schema_response = ['oneOf' => $schema_response];
    }
    // Insert reference of response.
    $path_method_spec['responses'][Response::HTTP_OK] = [
      'description' => $this->t('A list of entities @type @bundle',
        [
          '@type' => $config_decoupled->getTargetEntityType(),
          '@bundle' => $bundle,
        ]),
      'content' => [
        "application/json" => [
          'schema' => [
            'type' => 'array',
            'items' => $schema_response,
          ],
        ],
      ],
    ];

    // Insert discriminator.
    if ($number_responses > 1) {
      // We don't need to specify the mapping cause normally the key in schema
      // is same as the key insert into payload.
      // @see https://swagger.io/specification/#discriminator-object
      $path_method_spec['responses'][Response::HTTP_OK]['content']['application/json']['schema']['discriminator'] = ['propertyName' => OpenApiDecoupledToolboxSubscriber::OPEN__API_DECOUPLED__TOOLBOX__ID_KEY];
    }
    $path_method_spec['summary'] = t('Decoupled toolbox endpoints');
    $path_method_spec['parameters'] = array_merge($path_method_spec['parameters'], $this->getQueryParameter($config_decoupled));

    $path_method_spec['operationId'] = 'decoupled_toolbox_' . $config_decoupled->getTargetEntityType() . '_' . $bundle . ':' . $method;
    // $path_method_spec['schemes'] = [$this->request->getScheme()];
    return $path_method_spec;
  }

  /**
   * Build query route parameter.
   *
   * @param \Drupal\openapi_decoupled_toolbox\Entity\OpenApiDecoupledToolbox|NULL $config_decoupled
   *   The config open api decoupled entity to generate query paramter.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getQueryParameter(OpenApiDecoupledToolbox $config_decoupled = NULL): array {
    // Generate for all open API decoupled entities if ommit.
    if (empty($config_decoupled)) {
      $config_decoupleds = $this->getDecoupledToolboxEnabledEntityTypes();
      $displays = [];
      foreach ($config_decoupleds as $config_decoup) {
        $displays = array_merge($displays, $config_decoup->getTargetDisplay());
      }
    }
    else {
      $displays = $config_decoupled->getTargetDisplay();
    }
    $parameters = [];
    // Set display with enum.
    $schema = ['type' => 'string'];
    if (!empty($displays)) {
      $schema['enum'] = array_values($displays);
    }
    $parameters[] = [
      'name' => 'display',
      'schema' => $schema,
      'in' => 'query',
    ];
    // Set offset.
    $parameters[] = [
      'name' => 'offset',
      'schema' => ['type' => 'integer'],
      'in' => 'query',
    ];
    // Set limit.
    $decoupled_settings = $this->configFactory->get('decoupled_toolbox.settings');
    $limit_settings = $decoupled_settings->get('limit');
    $required = !empty($limit_settings['required']);
    if (!empty($limit_settings['unlimited'])) {
      $required = FALSE;
    }
    $limit_param = [
      'name' => 'limit',
      'schema' => ['type' => 'integer'],
      'in' => 'query',
      'required' => $required,
    ];
    if ($required) {
      $limit_param['default'] = $limit_settings['value'] ?? 1;
    }
    $parameters[] = $limit_param;
    return $parameters;
  }

  /**
   * Build properties for entity reference.
   *
   * @param array $field
   *   The content field of display.
   * @param string $field_name
   *   The drupal field name of the data.
   * @param array $field_defintiions
   * @param array $definitions
   *   The open API array schema definitions.
   * @param array $parent
   *   An array of parent entities if it's entity reference.
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function buildEntityReferenceProperties(array $field, $field_name, array $field_defintiions, array &$definitions, array $parent): bool {
    $possibilities = $this->getReferencePossibility($field, $field_name, $field_defintiions);
    if ($possibilities === FALSE) {
      return FALSE;
    }
    foreach ($possibilities as $possibility) {
      $this->buildDefinitionEntityViewMode($possibility['type'], $possibility['bundle'], $possibility['display'], $definitions, $parent);
    }
    return TRUE;
  }

  /**
   * Get entity type and bund possibilities to be referenced by the field.
   *
   * @param array $field
   *   The content field of display.
   * @param string $field_name
   *   The drupal field name of the data.
   * @param array $field_defintiions
   *
   * @return array|false
   */
  private function getReferencePossibility(array $field, $field_name, array $field_defintiions) {
    if ($field['type'] !== 'decoupled_entity_reference') {
      return FALSE;
    }
    // No view mode.
    if (empty($field["settings"]["decoupled_entity_view_mode"])) {
      return FALSE;
    }
    $display = $field["settings"]["decoupled_entity_view_mode"];
    // No field definitions.
    if (empty($field_defintiions[$field_name])) {
      return FALSE;
    }
    /** @var $field_defintiionssd \Drupal\field\Entity\FieldConfig */
    $settings_field = $field_defintiions[$field_name]->getSettings();
    // @todo look for default: how to tranform via Drupal.
    $target_type = $settings_field['handler'] ? str_replace('default:', '', $settings_field['handler']) : '';
    $bundles = $settings_field['handler_settings']['target_bundles'] ?? [];
    $result = [];
    foreach ($bundles as $bundle) {
      $result[] = [
        'type' => $target_type,
        'bundle' => $bundle,
        'display' => $display,
      ];
    }
    return $result;
  }

  /**
   * Build properties with re location option.
   *
   * @param array $field
   *   The content field of display.
   * @param string $field_name
   *   The drupal field name of the data.
   * @param array $location
   * @param string $final_key
   *   The finale key alias.
   *
   * @return array|string[]
   */
  private function buildLocationProperties($field, $field_name, array &$location, $final_key): array {
    array_pop($location);
    $properties = [
      'type' => 'object',
    ];
    if (!empty($location)) {
      if (!empty($properties['properties'][current($location)])) {
        $temp_prop = $this->buildLocationProperties($field, $field_name, $location, $final_key);
        if (empty($properties['properties'][current($location)]['properties'])) {
          $properties['properties'][current($location)]['properties'] = [];
        }
        $properties['properties'][current($location)]['properties'] = array_merge($properties['properties'][current($location)]['properties'], $temp_prop['properties']);
        return $properties;
      }
      $properties['properties'][current($location)] = $this->buildLocationProperties($field, $field_name, $location, $final_key);
      return $properties;
    }
    if (empty($properties['properties'][$final_key])) {
      $properties['properties'][$final_key] = [];
    }
    // Get settings ouput formatter.
    $this->buildItemField($field, $field_name, $properties['properties'][$final_key], $final_key);

    return $properties;
  }

  /**
   * Build final item of properties.
   *
   * @param array $field
   *   The content field of display.
   * @param string $field_name
   *   The drupal field name of the data.
   * @param array $properties
   *   The properties array.
   * @param string $final_key
   *   The finale key alias.
   *
   * @return void|null
   */
  private function buildItemField(array $field, $field_name, array &$properties, $final_key) {
    if (empty($field['settings'][DecoupledFormatterBase::SETTINGS__DECOUPLED_FIELD_OUPUT])) {
      return NULL;
    }
    $settings_ouput = $field['settings'][DecoupledFormatterBase::SETTINGS__DECOUPLED_FIELD_OUPUT];

    // Direct field type.
    if (!is_array($settings_ouput)) {

      $properties = [
        'type' => $settings_ouput ?? 'string',
        'title' => t('"@name" alias of field "@field"', [
          '@name' => $final_key,
          '@field' => $field_name,
        ]),
      ];
      return NULL;
    }
    // Array of only one type field.
    if (count($settings_ouput) == 1) {
      $properties = [
        'type' => 'array',
        'items' => [
          'type' => current($settings_ouput) ?? 'string',
          'title' => t('"@name" alias of field "@field"', [
            '@name' => $final_key,
            '@field' => $field_name,
          ]),
        ],
      ];
      return NULL;
    }
    // Array with keys.
    $this->buildObjectFieldItem($properties, $field_name, $settings_ouput);
  }

  /**
   * Build an object properties recurcively.
   *
   * @param array $properties
   *   The properties array.
   * @param string $field_name
   *   The drupal field name of the data.
   * @param array $settings_ouput
   *   An arrray mapping of openapi output definition.
   *
   * @return void
   */
  private function buildObjectFieldItem(array &$properties, $field_name, array $settings_ouput): void {
    $properties = [
      'type' => 'object',
    ];
    foreach ($settings_ouput as $output_key => $ouput_type) {
      if (is_array($ouput_type)) {
        $properties['properties'][$output_key] = [];
        $this->buildObjectFieldItem($properties['properties'][$output_key], $output_key, $ouput_type);
        continue;
      }
      $properties['properties'][$output_key] = [
        'type' => $ouput_type,
        'title' => t('"@name" alias of field "@field"', [
          '@name' => $output_key,
          '@field' => $field_name,
        ]),
      ];
    }
  }

  /**
   * Build array of ref schema entities for 200 status response.
   *
   * @param array $definitions
   *   The open API array schema definitions.
   * @param string $entity_type
   * @param string $bundle
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRefSchemaEntityType(array $definitions, $entity_type = NULL, $bundle = NULL): array {
    $ref = [];
    $decoupled_toolbox_entity = $this->getDecoupledToolboxEnabledEntityTypes();
    foreach ($definitions as $key_def => $definition) {
      $find_def = FALSE;
      // Display only the entity set on config.
      foreach ($decoupled_toolbox_entity as $decoupled) {
        $target_entity_type = $decoupled->getTargetEntityType();
        if (!empty($entity_type) && $entity_type != $target_entity_type) {
          continue;
        }
        $target_bundles = $decoupled->getTargetBundle();
        $displays = $decoupled->getTargetDisplay();
        foreach ($target_bundles as $target_bundle) {
          if (!empty($entity_type) && $bundle != $target_bundle) {
            continue;
          }
          foreach ($displays as $display) {
            if ($key_def == $target_entity_type . '.' . $target_bundle . '.' . $display) {
              $find_def = TRUE;
              break(3);
            }
          }
        }
      }
      if ($find_def) {
        $ref[] = ['$ref' => '#/components/schemas/' . $key_def];
      }
    }
    return $ref;
  }

  /**
   * Get the error responses.
   *
   * @see https://swagger.io/specification/#responses-object
   *
   * @return array
   *   Keys are http codes. Values responses.
   */
  protected function getErrorResponses(): array {
    $responses[Response::HTTP_BAD_REQUEST] = [
      'description' => Response::$statusTexts[Response::HTTP_BAD_REQUEST],
      'content' => [
        'text/plain' => [
          'schema' => [
            'type' => 'string',
            'example' => 'offset parameter is undefined.',
          ],
        ],
      ],
    ];

    $responses[Response::HTTP_NOT_FOUND] = [
      'description' => Response::$statusTexts[Response::HTTP_NOT_FOUND],
      'content' => [
        'text/plain' => [
          'schema' => [
            'type' => 'string',
            'example' => $this->t('Could not retrieve content'),
          ],
        ],
      ],
    ];
    $responses[Response::HTTP_NOT_IMPLEMENTED] = [
      'description' => Response::$statusTexts[Response::HTTP_NOT_IMPLEMENTED],
      'content' => [
        'text/plain' => [
          'schema' => [
            'type' => 'string',
            'example' => $this->t('Unavailable decoupled view display'),
          ],
        ],
      ],
    ];
    $responses[Response::HTTP_INTERNAL_SERVER_ERROR] = [
      'description' => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
      'content' => [
        'text/plain' => [
          'schema' => [
            'type' => 'string',
            'example' => Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR],
          ],
        ],
      ],
    ];
    return $responses;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiName() {
    return $this->t('Decoupled toolbox API');
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiDescription() {
    return $this->t('The Decoupled Toolobx API provide by the Decoupled toolbox module.');
  }

}
