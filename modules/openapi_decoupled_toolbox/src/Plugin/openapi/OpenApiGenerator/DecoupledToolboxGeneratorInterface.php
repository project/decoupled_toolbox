<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator;

/**
 * Define the interface for plugin openapi of mopenapi_decoupled_toolbox.
 */
interface DecoupledToolboxGeneratorInterface {

  /**
   * Return the responses schema.
   *
   * @param array $definitions
   *
   * @return mixed
   */
  public function getPathsResponses(array $definitions);

}
