<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\Derivative;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator\DecoupledToolboxGenerator;
use Drupal\openapi_decoupled_toolbox\Plugin\openapi\OpenApiGenerator\DecoupledToolboxRestGenerator;

/**
 * Provides local action definitions for all entity bundles.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class OpenAPiDecoupledToolboxDeriver extends DeriverBase implements ContainerDeriverInterface {
  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $this->derivatives = [];
    $base_plugin_definition['class'] = DecoupledToolboxGenerator::class;
    // Change class of plugin openapi definitions for rest.
    if ($this->moduleHandler->moduleExists('rest')) {
      $base_plugin_definition['class'] = DecoupledToolboxRestGenerator::class;
    }
    $this->derivatives[] = $base_plugin_definition;
    return $this->derivatives;
  }

}
