<?php

namespace Drupal\openapi_decoupled_toolbox\Plugin\Block;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\openapi_decoupled_toolbox\OpenapiDecoupledToolboxTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Menu' Block for OpenApi decoupled toolbox.
 *
 * @Block(
 *   id = "oapi_dt_menu_block",
 *   admin_label = @Translation("Menu block"),
 *   label = @Translation("OpenAPI decoupled toolbox"),
 *   category = @Translation("OpenAPI decoupled toolbox"),
 * )
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class MenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * An instance of current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * An instance of entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An instance of open api ui manger.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface|null
   */
  protected $openapiUiManager;

  use OpenapiDecoupledToolboxTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $current_route_match, EntityTypeManagerInterface $entity_type_manager, PluginManagerInterface $openapi_ui_manager = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->openapiUiManager = $openapi_ui_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $ui_manager = NULL;
    $module_handler = $container->get('module_handler');
    if ($module_handler->moduleExists('openapi_ui')) {
      $ui_manager = $container->get('plugin.manager.openapi_ui.ui');
    }
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $ui_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function baseConfigurationDefaults(): array {
    $config = parent::baseConfigurationDefaults();
    // Hide title by default.
    $config['label_display'] = FALSE;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $entity_type = $this->currentRouteMatch->getParameter('entity_type_id');
    $bundle = $this->currentRouteMatch->getParameter('bundle');
    $display = $this->currentRouteMatch->getParameter('view_mode_name');
    $config_decoupled = $this->getDecoupledToolboxEnabledEntityTypes($entity_type, [$bundle], [$display]);
    $links = [];
    $links['add_config'] = [
      'title' => $this->t('Add OpenAPI'),
      'url' => Url::fromRoute('entity.openapi_decoupled_toolbox.add_form', [], [
        'query' => [
          'entity_type' => $entity_type,
          'bundle' => $bundle,
          'display' => $display,
        ],
      ]),
    ];
    if (!empty($config_decoupled)) {
      $this->generateDecoupledTollboxLink($config_decoupled, $links);
    }
    $this->generateOpenApiLink($links, $entity_type, $bundle, $config_decoupled);
    return [
      '#type' => 'dropbutton',
      '#dropbutton_type' => 'big',
      '#attributes' => ['class' => ['dropbutton--big']],
      '#links' => $links,
    ];
  }

  /**
   * Generate link for config openapi decoupled toolbox entity.
   *
   * @param array $config_decoupled
   *   An array of config openapi deocupled toolbox entities.
   * @param array $links
   *   An array of links.
   *
   * @return false|void
   *   False if error.
   */
  private function generateDecoupledTollboxLink(array $config_decoupled, array &$links) {
    if (empty($config_decoupled)) {
      return FALSE;
    }
    unset($links['add_config']);
    $delta_link = 0;
    foreach ($config_decoupled as $config) {
      $links['config_decoupled_' . $delta_link] = [
        'title' => $this->t('Edit OpenAPI'),
        'url' => Url::fromRoute('entity.openapi_decoupled_toolbox.edit_form', ['openapi_decoupled_toolbox' => $config->id()]),
      ];
      $delta_link++;
    }
  }

  /**
   * Generate link for openApi.
   *
   * @param array $links
   *   An array of links.
   * @param string $entity_type
   *   The entity type to generate link.
   * @param string $bundle
   *   The bundle to generate link.
   * @param array $config_decoupled
   *   The openapi decoupled toolbox entity config.
   *
   * @return void
   */
  private function generateOpenApiLink(array &$links, $entity_type, $bundle, array $config_decoupled): void {
    if ($this->openapiUiManager !== NULL) {
      $ui_plugins = $this->openapiUiManager->getDefinitions();
      $query_arg = [];
      // Add query options only if decoupled has been configured.
      if (!empty($config_decoupled)) {
        $query_arg = [
          'query' => [
            'options' => [
              'entity_type_id' => $entity_type,
              'bundle_name' => $bundle,
            ],
          ],
        ];
      }
      foreach ($ui_plugins as $ui_plugin_id => $ui_plugin) {
        $interface_args = [
          'openapi_generator' => 'decoupled_toolbox',
          'openapi_ui' => $ui_plugin_id,
        ];

        $title = $this->t('Explore %interface', ['%interface' => $ui_plugin['label']]);
        if (!empty($config_decoupled)) {

          $title = $this->t('@type @bundle @interface', [
            '@interface' => $ui_plugin['label'],
            '@type' => $entity_type,
            '@bundle' => $bundle,
          ]);
        }
        $links[$ui_plugin_id] = [
          'url' => Url::fromRoute('openapi.documentation', $interface_args, $query_arg),
          'title' => $title,
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    $tags = ['url.path'];
    // Add tags cache decoupled toolbox config.
    return Cache::mergeTags(parent::getCacheContexts(), $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $entity_type = $this->currentRouteMatch->getParameter('entity_type_id');
    $bundle = $this->currentRouteMatch->getParameter('bundle');
    $display = $this->currentRouteMatch->getParameter('view_mode_name');
    $config_decoupled = $this->getDecoupledToolboxEnabledEntityTypes($entity_type, [$bundle], [$display]);
    // When new config is add.
    $tags = ['config:openapi_decoupled_toolbox_list'];
    if (!empty($config_decoupled)) {
      // Add cache tags config entity.
      foreach ($config_decoupled as $config) {
        $tags[] = 'config:openapi_decoupled_toolbox.oa_dt.' . $config->id();
      }
    }
    // Add tags cache decoupled toolbox config.
    return Cache::mergeTags(parent::getCacheTags(), $tags);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account, $return_as_object = FALSE) {
    $current_route = $this->currentRouteMatch->getRouteName();
    return AccessResult::allowedIf(strpos($current_route, 'entity.entity_view_display.') === 0);
  }

}
