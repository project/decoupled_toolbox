<?php

namespace Drupal\decoupled_toolbox\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $moduleHandler = \Drupal::service('module_handler');
    // Disable route controller if rest module is enable.
    if ($moduleHandler->moduleExists('rest') && $route = $collection->get('decoupled_toolbox.entity_decoupled_data.collection')) {
      $route->setRequirement('_access', 'FALSE');
      $route->setPath('/decoupled-toolbox/depreciated');
    }
  }

}
