<?php

namespace Drupal\decoupled_toolbox\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Extends the base Event for when the rendered output is ready for alteration.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class OnRenderedOutputBuiltEvent extends Event {

  /**
   * Array of the output.
   *
   * @var array
   */
  protected $renderedOutput;

  /**
   * Entity linked to this event.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Computed cache tags for this event.
   *
   * @var array
   */
  protected $cacheTags;

  /**
   * Display used for this rendering.
   *
   * @var string
   */
  protected $display;

  /**
   * OnOutputBuiltEvent constructor.
   *
   * @param array $renderedOutput
   *   The rendered output to alter.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity linked to this event.
   * @param array $cache_tags
   *   Computed cache tags.
   * @param string $display
   *   Display used for this rendering.
   */
  public function __construct(array &$renderedOutput, EntityInterface $entity, array &$cache_tags, $display) {
    $this->renderedOutput = &$renderedOutput;
    $this->cacheTags = &$cache_tags;
    $this->entity = $entity;
    $this->display = $display;
  }

  /**
   * Gets the output.
   *
   * @return array
   *   The rendered output, passed by reference for direct edit.
   */
  public function &getRenderedOutput(): array {
    return $this->renderedOutput;
  }

  /**
   * Replaces the rendered output.
   *
   * @param array $newRenderedOutput
   *   The new rendered output; replaces the old referenced array.
   */
  public function setRenderedOutput(array &$newRenderedOutput): void {
    $this->renderedOutput = &$newRenderedOutput;
  }

  /**
   * Get entity of the render.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity which is rendering.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Set entity of the render.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which is rendering.
   */
  public function setEntity(EntityInterface $entity): void {
    $this->entity = $entity;
  }

  /**
   * Get display of the render.
   *
   * @return string
   *   The display of event.
   */
  public function getDisplay(): string {
    return $this->display;
  }

  /**
   * Set display of the render.
   *
   * @param string $display
   *   The display to set.
   */
  public function setDisplay($display): void {
    $this->display = $display;
  }

  /**
   * Get cache tags for the event.
   *
   * @return array
   *   Cache tags.
   */
  public function getCacheTags(): array {
    return $this->cacheTags;
  }

  /**
   * Set cache tags of the render.
   *
   * @param array $cache_tags
   *   Cache tags. The reference will be used.
   */
  public function setCacheTags(array &$cache_tags): void {
    $this->cacheTags = &$cache_tags;
  }

}
