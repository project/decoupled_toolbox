<?php

namespace Drupal\decoupled_toolbox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\decoupled_toolbox\Service\RequestEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for entity decoupled data.
 */
abstract class DecoupledDataControllerBase extends ControllerBase {

  /**
   * Decoupled request service.
   *
   * @var \Drupal\decoupled_toolbox\Service\RequestEntityInterface
   */
  protected $decoupledRequestEntity;

  /**
   * Constructor.
   *
   * @param \Drupal\decoupled_toolbox\Service\RequestEntityInterface $decoupled_request_entity
   *   Decoupled request service.
   */
  public function __construct(RequestEntityInterface $decoupled_request_entity) {
    $this->decoupledRequestEntity = $decoupled_request_entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('decoupled.request.entity')
    );
  }

}
