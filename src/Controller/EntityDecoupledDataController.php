<?php

namespace Drupal\decoupled_toolbox\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Service\RequestEntityInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

/**
 * Controller for entity decoupled data.
 */
class EntityDecoupledDataController extends DecoupledDataControllerBase {

  /**
   * Called by the route decoupled_toolbox.entity_decoupled_data.collection.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   JSON response containing entities' values, or standard Response on
   *   errors.
   */
  public function collection(Request $request, $type, $bundle) {
    if ($request->getMethod() !== Request::METHOD_GET) {
      // Should never happen, because methods are filtered on the routing side.
      throw new MethodNotAllowedException([Request::METHOD_GET]);
    }
    try {
      $collection = $this->decoupledRequestEntity->getCollection($request, $type, $bundle);
      $response = new CacheableJsonResponse($collection['render']);
      if (!empty($collection['cache_tags'])) {
        $cacheMetadata = new CacheableMetadata();
        $cacheMetadata->setCacheTags($collection['cache_tags']);
        $cacheMetadata->setCacheContexts(['url.query_args']);
        $response->addCacheableDependency($cacheMetadata);
      }
      return $response;
    }
    catch (InvalidParameterException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new Response($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_BAD_REQUEST);
    }
    catch (CouldNotRetrieveContentException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new Response($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_NOT_FOUND);
    }
    catch (UnavailableDecoupledViewDisplayException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new Response($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_NOT_IMPLEMENTED);
    }
    catch (QueryException $exception) {
      // Failure on query execution or preparation.
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new Response(Settings::get(RequestEntityInterface::SETTINGS__STATE__DEBUG_ENABLED) ? $this->t('There was an error in the query: @message.', ['@message' => $exception->getMessage()]) : NULL, Response::HTTP_BAD_REQUEST);
    }
    catch (\Exception $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new Response($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

}
