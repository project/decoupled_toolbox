<?php

namespace Drupal\decoupled_toolbox\Controller;

/**
 * Interface for classes using FilterTrait.
 */
interface FilterInterface {

  public const EVENT__CONDITION_PREPROCESS = 'EVENT__CONDITION_PREPROCESS';

}
