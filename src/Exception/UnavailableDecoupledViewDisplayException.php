<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * Tried to work on an entity without the decoupled view display.
 */
class UnavailableDecoupledViewDisplayException extends \Exception {}
