<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * A formatter was set on a field type which is not compatible.
 */
class UnexpectedFormatterException extends \Exception {}
