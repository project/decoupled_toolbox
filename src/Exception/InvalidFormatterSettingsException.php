<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * A formatter setting is invalid.
 */
class InvalidFormatterSettingsException extends \Exception {}
