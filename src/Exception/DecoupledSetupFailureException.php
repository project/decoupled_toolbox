<?php

namespace Drupal\decoupled_toolbox\Exception;

/**
 * When decoupled view mode or display could not be created for some reason.
 */
class DecoupledSetupFailureException extends \Exception {}
