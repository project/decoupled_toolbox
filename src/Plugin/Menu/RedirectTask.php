<?php

namespace Drupal\decoupled_toolbox\Plugin\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Class RedirectTask alter action decoupled toolbox.
 */
class RedirectTask extends LocalActionDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    // Inject entity type and bundle inside link action.
    $entity_type = $route_match->getParameter('entity_type_id');
    $bundle = $route_match->getParameter('bundle');
    if (empty($bundle)) {
      $entity_type = $bundle;
    }
    return [
      'type' => $entity_type,
      'bundle' => $bundle,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(RouteMatchInterface $route_match) {
    // Inject query option of decoupled toolbox inside link action.
    $options = (array) $this->pluginDefinition['options'];
    $display = $route_match->getParameter('view_mode_name');

    // Check if destination should be appended to the link.
    if (!empty($display)) {
      $options['query']['display'] = $display;
    }
    $options['attributes']['target'] = '_blank';
    return $options;
  }

}
