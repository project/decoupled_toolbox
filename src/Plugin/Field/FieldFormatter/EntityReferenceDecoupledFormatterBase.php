<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Base plugin implementation of the decoupled entity formatters.
 */
abstract class EntityReferenceDecoupledFormatterBase extends DecoupledFormatterBase {

  /**
   * Checks if missing entity reference should be ignored.
   *
   * @return bool
   *   TRUE if missing entity reference should be ignored, FALSE otherwise.
   */
  protected function ignoreMissingEntityReference(): bool {
    return $this->getDecoupledToolboxConfig('ignore_missing_entity_reference', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, string $langCode): FieldValueAndOptions {
    $this->validateSettingsOnRender();

    $decoupledFieldKey = $this->getDecoupledFieldKey();
    $decoupledFieldLocation = $this->getDecoupledFieldLocation();

    $collectedCacheableMetadata = NULL;

    if ($this->getDecoupledForcedMultipleOutput() || $this->isMultivaluedFieldItemList($items)) {
      $processedMultiValuedItems = $this->viewMultivaluedFieldItemList($items, $collectedCacheableMetadata);

      if (empty($processedMultiValuedItems)) {
        // No value set.
        return $this->getNoValueReturn($collectedCacheableMetadata);
      }

      return new FieldValueAndOptions($decoupledFieldKey, $processedMultiValuedItems, $decoupledFieldLocation, $collectedCacheableMetadata);
    }

    try {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      $item = $items->first();
    }
    catch (MissingDataException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());

      // No value set.
      return $this->getNoValueReturn();
    }

    if (empty($item)) {
      // No value set.
      return $this->getNoValueReturn();
    }

    $viewFieldItem = $this->viewFieldItem($item, $collectedCacheableMetadata);

    return new FieldValueAndOptions($decoupledFieldKey, $viewFieldItem, $decoupledFieldLocation, $collectedCacheableMetadata);
  }

  /**
   * {@inheritdoc}
   */
  protected function viewMultivaluedFieldItemList(FieldItemListInterface $items, ?CacheableMetadata $collectedCacheableMetadata = NULL): array {
    if (!$items instanceof EntityReferenceFieldItemListInterface) {
      throw new UnexpectedFormatterException('Tried to render an entity field list, but the field does not implement EntityReferenceFieldItemListInterface.');
    }

    $data = [];

    foreach ($items as $item) {
      try {
        $data[] = $this->viewFieldItem($item, $collectedCacheableMetadata);
      }
      catch (InvalidContentException $exception) {
        // This happens when the reference was deleted.
        if ($this->ignoreMissingEntityReference()) {
          continue;
        }

        // Rethrow the exception.
        throw $exception;
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    return [];
  }

}
