<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract of a decoupled formatter.
 */
abstract class DecoupledFormatterBase extends FormatterBase implements DecoupledFormatterInterface {

  public const SETTINGS__DECOUPLED_FIELD_KEY = 'decoupled_field_key';

  public const SETTINGS__DECOUPLED_FIELD_LOCATION = 'decoupled_field_location';

  public const SETTINGS__DECOUPLED_FIELD_OUPUT = 'decoupled_field_output';

  public const SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY = 'decoupled_field_hide_empty';

  public const SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT = 'decoupled_forced_multiple_output';

  use LoggerChannelTrait;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Decoupled formatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('module_handler'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        self::SETTINGS__DECOUPLED_FIELD_KEY => '',
        self::SETTINGS__DECOUPLED_FIELD_LOCATION => '',
        self::SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY => TRUE,
        self::SETTINGS__DECOUPLED_FIELD_OUPUT => static::getOutputDefinitions(),
        self::SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * Use this method to return an empty render value.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   *   Cacheable metadata.
   *
   * @return \Drupal\decoupled_toolbox\FieldValueAndOptions
   *   The array to return.
   */
  protected function getNoValueReturn(?CacheableMetadata $cacheableMetadata = NULL): FieldValueAndOptions {
    return new FieldValueAndOptions($this->getDecoupledFieldKey(), NULL, $this->getDecoupledFieldLocation(), $cacheableMetadata);
  }

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    return '';
  }

  /**
   * Standard text escape for decoupled formatters.
   *
   * You may override this function to change or disable escapes in inheriting
   * classes.
   *
   * @param string $stringToEscape
   *   String to escape.
   *
   * @return string
   *   String escaped for output.
   */
  protected function escapeOutput($stringToEscape): string {
    return nl2br(Html::escape($stringToEscape));
  }

  /**
   * Shortcut to get decoupled_field_key value.
   *
   * @return string
   *   The decoupled_field_key value.
   */
  protected function getSettingsHideEmpty(): string {
    return $this->settings[self::SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY];
  }

  /**
   * Shortcut to get decoupled_field_key value.
   *
   * @return string
   *   The decoupled_field_key value.
   */
  protected function getDecoupledFieldKey(): string {
    return $this->settings[self::SETTINGS__DECOUPLED_FIELD_KEY];
  }

  /**
   * Shortcut to get decoupled_field_location value.
   *
   * @return string
   *   The decoupled_field_location value.
   */
  protected function getDecoupledFieldLocation(): string {
    return $this->settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION];
  }

  /**
   * Shortcut to get decoupled_forced_multiple_output value.
   *
   * @return bool
   *   The decoupled_forced_multiple_output value.
   */
  protected function getDecoupledForcedMultipleOutput(): bool {
    return $this->settings[self::SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT];
  }

  /**
   * Gets the decoupled_toolbox module main configuration.
   *
   * @param string $key
   *   The key to get from the configuration. Leave empty to get the whole
   *   configuration.
   * @param mixed $default
   *   The default value to return if there is no configuration. Defaults to
   *   NULL.
   *
   * @return array|mixed|null
   *   The configuration value or the whole configuration array.
   */
  protected function getDecoupledToolboxConfig(string $key = '', $default = NULL) {
    return \Drupal::configFactory()->get('decoupled_toolbox.settings')->get($key) ?? $default;
  }

  /**
   * Gets the settings summary array.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Array of markup.
   */
  public function settingsSummary(): array {
    $settings = $this->getSettings();

    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup[] $summary */
    $summary = parent::settingsSummary();

    if (!empty($settings[self::SETTINGS__DECOUPLED_FIELD_KEY])) {
      $summary[] = $this->t('Decoupled field ID: <strong>@id</strong>', ['@id' => $settings[self::SETTINGS__DECOUPLED_FIELD_KEY]]);
    }
    else {
      $summary[] = $this->t('<span class="error messages--error">⚠️ Decoupled field ID: <em>️undefined</em></span>');
    }

    if (!empty($settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION])) {
      $summary[] = $this->t('Decoupled field location: <strong>@location</strong>', ['@location' => $settings[self::SETTINGS__DECOUPLED_FIELD_LOCATION]]);
    }

    if ($settings[self::SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY]) {
      $summary[] = $this->t('The field is hidden if value is empty');
    }

    if ($settings[self::SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT]) {
      $summary[] = $this->t('The forced multiple output is active');
    }

    return $summary;
  }

  /**
   * Checks whether a FieldItemList instance comes from a multi-valued field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The FieldItemList instance to test.
   *
   * @return bool
   *   TRUE if multi-valued, FALSE if single valued.
   */
  protected function isMultivaluedFieldItemList(FieldItemListInterface $items): bool {
    return $items->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->isMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $field_definition = $this->fieldDefinition;
    $entity_type = $field_definition->getTargetEntityTypeId();
    if ($entity_type === 'taxonomy_term') {
      $entity_type = 'term';
    }

    $form[self::SETTINGS__DECOUPLED_FIELD_KEY] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FIELD_KEY),
      '#description' => $this->t('This value replaces the Drupal field ID by a suitable key for the decoupled display. Supports any character as defined by the JSON key syntax.'),
      '#required' => TRUE,
      '#title' => $this->t('Decoupled field key'),
      '#type' => 'textfield',
    ];

    $form[self::SETTINGS__DECOUPLED_FIELD_LOCATION] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FIELD_LOCATION),
      '#description' => $this->t('Location of this field. Leave empty for default position. Examples: <ul><li><strong>/absolute/location/in/final/output</strong>: absolute position, the field will be placed in the object keyed by "output"</li><li><strong>/0/a_key_name</strong>: inside an object keyed by "a_key_name" in the first object of the output.</li></ul>'),
      '#title' => $this->t('Decoupled field location'),
      '#type' => 'textfield',
    ];

    if ($this->moduleHandler->moduleExists('token')) {
      $form['token'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$entity_type],
        '#show_restricted' => TRUE,
        '#global_types' => TRUE,
      ];
    }

    $form[self::SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FIELD_HIDE_EMPTY),
      '#description' => $this->t('If this option is checked the field will be not output if the value is empty.'),
      '#title' => $this->t('Hide if empty'),
      '#type' => 'checkbox',
    ];
    $form[self::SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT] = [
      '#default_value' => $this->getSetting(self::SETTINGS__DECOUPLED_FORCED_MULTIPLE_OUTPUT),
      '#description' => $this->t('This option forces this field to output as multiple even if it is a single valued field.'),
      '#title' => $this->t('Force multiple output'),
      '#type' => 'checkbox',
    ];

    return $form;
  }

  /**
   * Validates the formatter settings when building the output.
   *
   * Override this in inheriting classes if the formatter needs to validate
   * other settings.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   */
  protected function validateSettingsOnRender(): array {
    $settings = $this->getSettings();

    if (empty($settings[self::SETTINGS__DECOUPLED_FIELD_KEY])) {
      throw new InvalidFormatterSettingsException();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    // Return an empty array because we are not using the default render stack.
    return [];
  }

  /**
   * Processes a single field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $collectedCacheableMetadata
   *   Will be populated with collected cacheable metadata from field items.
   *
   * @return mixed|\Drupal\decoupled_toolbox\FieldValueAndOptions
   *   Either a FieldValueAndOptions instance with metadata, or the directly
   *   pre-processed rendered value.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   An error occurred about the content of the field.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   * @throws \Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException
   *   The formatter is not compatible with the field.
   */
  abstract protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL);

  /**
   * Processes field with cardinality > 1.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Items to format.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $collectedCacheableMetadata
   *   Will be populated with collected cacheable metadata from field items.
   *
   * @return array
   *   The formatted items as an array.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   An error occurred about the content of the field.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   The formatter settings are invalid.
   * @throws \Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException
   *   The formatter is not compatible with the field.
   */
  abstract protected function viewMultivaluedFieldItemList(FieldItemListInterface $items, ?CacheableMetadata $collectedCacheableMetadata = NULL): array;

}
