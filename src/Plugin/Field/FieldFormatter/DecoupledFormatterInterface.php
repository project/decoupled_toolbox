<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Decoupled formatter interface.
 */
interface DecoupledFormatterInterface {

  /**
   * Builds decoupled output for a field.
   *
   * Call $this->validateSettingsOnRender() before processing the values to
   * validate the formatter settings.
   *
   * The processed field cache tags may be set from this function.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langCode
   *   The language that should be used to render the field.
   *
   * @return \Drupal\decoupled_toolbox\FieldValueAndOptions
   *   An object containing the pre-processed decoupled value and metadata.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   Error when processing a field value.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException
   *   Wrong settings on the formatter.
   * @throws \Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException
   *   The formatter is not compatible with the field.
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, string $langCode): FieldValueAndOptions;

  /**
   * Return the structure of output formatter.
   *
   * @return array|string
   *   Usually a keyed array or direct value (integer, string...).
   */
  public static function getOutputDefinitions();

}
