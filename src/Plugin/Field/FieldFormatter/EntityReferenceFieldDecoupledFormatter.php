<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;

/**
 * Plugin implementation of the 'decoupled_entity_reference_field' formatter.
 *
 * This formatter renders the field of a referenced entity.
 *
 * @FieldFormatter(
 *   id = "decoupled_entity_reference_field",
 *   label = @Translation("Entity reference field decoupled formatter"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   }
 * )
 */
class EntityReferenceFieldDecoupledFormatter extends EntityReferenceDecoupledFormatterBase {

  protected const SETTINGS__TARGET_FIELD_ID = 'target_field_id';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        self::SETTINGS__TARGET_FIELD_ID => '',
      ] + parent::defaultSettings();
  }

  /**
   * Gets the settings summary array.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Array of markup.
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    $targetFieldId = $this->getTargetFieldId();

    if (!empty($targetFieldId)) {
      $summary[] = $this->t('Target field ID: <strong>@id</strong>', ['@id' => $targetFieldId]);
      return $summary;
    }

    $summary[] = $this->t('⚠️ Target field ID: <em>️undefined</em>');
    return $summary;
  }

  /**
   * Shortcut to get target_field_id value.
   *
   * @return string
   *   The target_field_id value, or empty string if not set.
   */
  protected function getTargetFieldId() {
    return !empty($this->settings[self::SETTINGS__TARGET_FIELD_ID]) ? $this->settings[self::SETTINGS__TARGET_FIELD_ID] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form[self::SETTINGS__TARGET_FIELD_ID] = [
      '#default_value' => $this->getTargetFieldId(),
      '#description' => $this->t('The ID of the field on the target entity from which the value will be useed for output. E.g.: <code>body</code>, <code>field_text</code>'),
      '#required' => TRUE,
      '#title' => $this->t('Target field ID'),
      '#type' => 'textfield',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateSettingsOnRender(): array {
    parent::validateSettingsOnRender();

    if (empty($this->getTargetFieldId())) {
      throw new InvalidFormatterSettingsException();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    if (!$item instanceof EntityReferenceItem) {
      throw new UnexpectedFormatterException($this->t('Tried to render an entity field item, but the given object does not implement EntityReferenceItem.'));
    }

    // $fieldValue is supposed to be an array containing at least the target_id
    // key, and the target_revision_id for entity reference revisions fields.
    $fieldValue = $item->getValue();

    if (empty($fieldValue)) {
      // This should never be possible.
      throw new InvalidContentException($this->t('Field value is empty.'));
    }

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $item->entity;

    if (empty($entity)) {
      // This happens when the reference was deleted.
      throw new InvalidContentException($this->t('Reference was deleted.'));
    }

    // Target field ID is already validated in validateSettingsOnRender().
    $targetFieldId = $this->getTargetFieldId();

    try {
      /** @var \Drupal\Core\Field\FieldItemListInterface $items */
      $items = $entity->get($targetFieldId);
    }
    catch (\InvalidArgumentException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
      // Field was not found.
      throw new InvalidContentException($this->t('Field @targetFieldId cannot be processed. Does the field exist? Typo?', ['@targetFieldId' => $targetFieldId]));
    }

    if ($this->isMultivaluedFieldItemList($items)) {
      // Multivalued fields will output inside an array, whatever the final
      // item count.
      $data = [];

      foreach ($items as $delta => $field_item) {
        $data[$delta] = $this->escapeOutput($field_item);
      }

      return $data;
    }

    try {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      $item = $items->first();
    }
    catch (MissingDataException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
      // The specified field could not be processed. Silently ignore this.
      return NULL;
    }

    if (empty($item)) {
      // No value set.
      return NULL;
    }

    // Register cacheable metadata of the referenced entity.
    $collectedCacheableMetadata && $collectedCacheableMetadata->merge(CacheableMetadata::createFromObject($entity));

    // Simply return the value.
    return $this->escapeOutput($item->value);
  }

}
