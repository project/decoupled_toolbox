<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;

/**
 * Plugin implementation of the 'decoupled_file' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_file",
 *   label = @Translation("File decoupled formatter"),
 *   field_types = {
 *     "file",
 *     "image",
 *   }
 * )
 */
class FileDecoupledFormatter extends EntityReferenceDecoupledFormatterBase {

  protected const SETTINGS__INCLUDE_MIME_TYPE = 'include_mime_type';

  protected const SETTINGS__INCLUDE_FILE_CONTENT = 'include_file_content';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return parent::defaultSettings() + [
        self::SETTINGS__INCLUDE_MIME_TYPE => FALSE,
        self::SETTINGS__INCLUDE_FILE_CONTENT => FALSE,
      ];
  }

  /**
   * Shortcut to get include_file_content value.
   *
   * @return bool
   *   The include_file_content value.
   */
  protected function getIncludeFileContentSettings(): bool {
    return $this->settings[self::SETTINGS__INCLUDE_FILE_CONTENT];
  }

  /**
   * Shortcut to get include_mime_type value.
   *
   * @return bool
   *   The include_mime_type value.
   */
  protected function getIncludeMimeTypeSettings(): bool {
    return $this->settings[self::SETTINGS__INCLUDE_MIME_TYPE];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form[self::SETTINGS__INCLUDE_MIME_TYPE] = [
      '#default_value' => $this->getSetting(self::SETTINGS__INCLUDE_MIME_TYPE),
      '#description' => $this->t('Check this option to include the MIME type of the file.'),
      '#title' => $this->t('Include MIME type'),
      '#type' => 'checkbox',
    ];

    $form[self::SETTINGS__INCLUDE_FILE_CONTENT] = [
      '#default_value' => $this->getSetting(self::SETTINGS__INCLUDE_FILE_CONTENT),
      '#description' => $this->t('Check this option to include the file content.'),
      '#title' => $this->t('Include file content'),
      '#type' => 'checkbox',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $settings = $this->getSettings();
    $summary = parent::settingsSummary();

    if ($settings[self::SETTINGS__INCLUDE_MIME_TYPE]) {
      $summary[] = $this->t('The MIME type of the file is shown');
    }

    if ($settings[self::SETTINGS__INCLUDE_FILE_CONTENT]) {
      $summary[] = $this->t('The file content is appended to the values.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    if (!$item instanceof EntityReferenceItem) {
      throw new UnexpectedFormatterException('Tried to render an entity field item, but the given object does not implement EntityReferenceItem.');
    }

    // $fieldValue is supposed to be an array containing at least the target_id
    // key and other values related to a file.
    $fieldValue = $item->getValue();

    if (empty($fieldValue)) {
      // This should never be possible.
      throw new InvalidContentException('Field value is empty.');
    }

    /** @var \Drupal\file\FileInterface $file */
    $file = $item->entity;

    if (empty($file)) {
      // This happens when the reference was deleted.
      throw new InvalidContentException('Reference was deleted.');
    }

    // URLs must be absolute because of decoupled.
    $url = $file->createFileUrl(FALSE);

    $collectedCacheableMetadata && $collectedCacheableMetadata->merge(CacheableMetadata::createFromObject($file));

    $itemValues = [
      'url' => $url,
    ];

    if ($this->getIncludeMimeTypeSettings()) {
      $itemValues['mime_type'] = $file->getMimeType();
    }

    if ($this->getIncludeFileContentSettings()) {
      $itemValues['file_content'] = file_get_contents($file->getFileUri());
    }

    return $itemValues;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    return [
      'file_content' => 'string|bool',
      'url' => 'string',
      'mime_type' => 'string',
    ];
  }

}
