<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\FieldValueAndOptions;

/**
 * Plugin implementation of the 'decoupled_generic' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_generic",
 *   label = @Translation("Generic decoupled formatter"),
 *   field_types = {
 *     "changed",
 *     "created",
 *     "comment",
 *     "daterange",
 *     "datetime",
 *     "decimal",
 *     "email",
 *     "file_uri",
 *     "float",
 *     "integer",
 *     "language",
 *     "list_float",
 *     "list_integer",
 *     "list_string",
 *     "map",
 *     "path",
 *     "password",
 *     "string",
 *     "string_long",
 *     "timestamp",
 *     "telephone",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "uri",
 *     "uuid",
 *   }
 * )
 */
class GenericDecoupledFormatter extends DecoupledFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    // @todo get field type.
    return 'string';
  }

  /**
   * {@inheritdoc}
   */
  public function viewElementsForDecoupled(FieldItemListInterface $items, $langCode): FieldValueAndOptions {
    $this->validateSettingsOnRender();

    $decoupledFieldKey = $this->getDecoupledFieldKey();
    $decoupledFieldLocation = $this->getDecoupledFieldLocation();

    $collectedCacheableMetadata = new CacheableMetadata();

    if ($this->isMultivaluedFieldItemList($items)) {
      $processedMultiValuedItems = $this->viewMultivaluedFieldItemList($items, $collectedCacheableMetadata);

      if (empty($processedMultiValuedItems)) {
        // No value set.
        return $this->getNoValueReturn($collectedCacheableMetadata);
      }

      return new FieldValueAndOptions($decoupledFieldKey, $processedMultiValuedItems, $decoupledFieldLocation, $collectedCacheableMetadata);
    }

    try {
      /** @var \Drupal\Core\Field\FieldItemInterface $item */
      $item = $items->first();
    }
    catch (MissingDataException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());

      // No value set.
      return $this->getNoValueReturn($collectedCacheableMetadata);
    }

    if (empty($item)) {
      // No value set.
      return $this->getNoValueReturn($collectedCacheableMetadata);
    }

    return new FieldValueAndOptions($decoupledFieldKey, $this->viewFieldItem($item, $collectedCacheableMetadata), $decoupledFieldLocation, $collectedCacheableMetadata);
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return $this->escapeOutput($item->value);
  }

  /**
   * {@inheritdoc}
   */
  protected function viewMultivaluedFieldItemList(FieldItemListInterface $items, ?CacheableMetadata $collectedCacheableMetadata = NULL): array {
    $data = [];

    foreach ($items as $delta => $item) {
      $data[$delta] = $this->viewFieldItem($item, $collectedCacheableMetadata);
    }

    return $data;
  }

}
