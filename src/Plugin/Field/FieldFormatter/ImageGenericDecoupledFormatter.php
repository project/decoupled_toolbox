<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'decoupled_generic_image' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_generic_image",
 *   label = @Translation("Generic image decoupled formatter"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class ImageGenericDecoupledFormatter extends FileDecoupledFormatter {

  protected const IMAGE_SOURCE = "gam";

  /**
   * Stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The constructor of image decoupled formatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   Stream wrapper manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    ModuleHandlerInterface $module_handler,
    StreamWrapperManagerInterface $stream_wrapper_manager) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
      $module_handler);
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager */
    $stream_wrapper_manager = $container->get('stream_wrapper_manager');

    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('module_handler'),
      $stream_wrapper_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    if (!$item instanceof ImageItem) {
      throw new UnexpectedFormatterException($this->t("Tried to render an image item, but the given object does not implement ImageItem."));
    }

    // $fieldValue is supposed to be an array containing at least the target_id
    // key and other values related to an image (width, height, alt, title).
    $fieldValue = $item->getValue();

    if (empty($fieldValue)) {
      // This should never be possible.
      throw new InvalidContentException($this->t("Field value is empty."));
    }

    /** @var \Drupal\file\FileInterface $file */
    $file = $item->entity;

    if (empty($file)) {
      // This happens when the reference was deleted.
      throw new InvalidContentException($this->t("Reference was deleted."));
    }

    // Remove the scheme from the URI and remove erroneous leading or trailing.
    $uri = $this->streamWrapperManager::getTarget($file->getFileUri());

    $collectedCacheableMetadata && $collectedCacheableMetadata->merge(CacheableMetadata::createFromObject($file));

    $itemValues = array_merge([
      "source" => self::IMAGE_SOURCE,
      "uri" => $uri,
    ], $this->getCommonImageProperties($item));

    if ($this->getIncludeMimeTypeSettings()) {
      $itemValues['mime_type'] = $file->getMimeType();
    }

    if ($this->getIncludeFileContentSettings()) {
      $itemValues['file_content'] = file_get_contents($file->getFileUri());
    }

    return $itemValues;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    return [
      'file_content' => 'string|bool',
      'source' => 'string',
      'uri' => 'string',
      'mime_type' => 'string',
      'title' => 'string',
      'alt' => 'string',
    ];
  }

  /**
   * Gets generic common properties such as title, alt.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $item
   *   The ImageItem currently processed by the processor.
   *
   * @return array
   *   Array containing string indexed values.
   */
  protected function getCommonImageProperties(ImageItem $item) {
    try {
      $title = $item->get("title")->getValue();
    }
    catch (MissingDataException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
      $title = NULL;
    }

    try {
      $alt = $item->get("alt")->getValue();
    }
    catch (MissingDataException $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
      $alt = NULL;
    }
    return [
      "title" => $title,
      "alt" => $alt,
    ];
  }

}
