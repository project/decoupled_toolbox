<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemInterface;

/**
 * Plugin implementation of the "decoupled_link_url" formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_link_url",
 *   label = @Translation("Link URL decoupled formatter"),
 *   field_types = {
 *     "link",
 *   }
 * )
 */
class LinkUrlDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewFieldItem(FieldItemInterface $item, ?CacheableMetadata $collectedCacheableMetadata = NULL) {
    // Object that holds information about a URL.
    $url = $item->getUrl();
    $url_string = $url->toString(TRUE);
    $collectedCacheableMetadata && $collectedCacheableMetadata->merge(CacheableMetadata::createFromObject($url_string));
    return $url_string->getGeneratedUrl();
  }

}
