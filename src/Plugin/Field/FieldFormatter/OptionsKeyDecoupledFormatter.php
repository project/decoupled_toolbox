<?php

namespace Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'decoupled_list_key' formatter.
 *
 * @FieldFormatter(
 *   id = "decoupled_list_key",
 *   label = @Translation("Decoupled Key"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class OptionsKeyDecoupledFormatter extends GenericDecoupledFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = (int) $item->value;
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $field_type = $this->fieldDefinition->get('field_type');
    // Set hidden settings depending the field type.
    $form[static::SETTINGS__DECOUPLED_FIELD_OUPUT] = [
      '#type' => 'hidden',
      '#value' => [str_replace('list_', '', $field_type)],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOutputDefinitions() {
    return ['integer'];
  }

}
