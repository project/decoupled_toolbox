<?php

namespace Drupal\decoupled_toolbox\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Service\RequestEntityInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Provides a Decoupled toolbox collection rest Resource.
 *
 * @todo rename the ID of the resource, without the typo (hook updates needed).
 *
 * @RestResource(
 *   id = "deoupled_toolbox_collection",
 *   label = @Translation("Decoupled toolobox Resource"),
 *   uri_paths = {
 *     "canonical" = "/decoupled-api/{type}/{bundle}/collection"
 *   }
 * )
 */
class CollectionResource extends ResourceBase {

  /**
   * An instance of decoupled request entity service.
   *
   * @var \Drupal\decoupled_toolbox\Service\RequestEntityInterface
   */
  protected $decoupledRequestEntity;

  /**
   * An instance of current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, Request $current_request, RequestEntityInterface $decoupled_request_entity) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->decoupledRequestEntity = $decoupled_request_entity;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('example_rest'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('decoupled.request.entity')
    );
  }

  /**
   * Responds to entity GET requests.
   *
   * @param string $type
   *   The entity type to render.
   * @param string $bundle
   *   The bundle to render.
   *
   * @return \Drupal\rest\ResourceResponse
   *   ResourceResponse object.
   */
  public function get($type, $bundle): ResourceResponse {
    try {
      $collection = $this->decoupledRequestEntity->getCollection($this->currentRequest, $type, $bundle);
      $response = new ResourceResponse();
      // Build response with caches.
      if (!empty($collection['render'])) {
        $response = new ResourceResponse($collection['render']);
        if (!empty($collection['cache_tags'])) {
          $cacheMetadata = new CacheableMetadata();
          $cacheMetadata->setCacheTags($collection['cache_tags']);
          $cacheMetadata->setCacheContexts(['url.query_args']);
          $response->addCacheableDependency($cacheMetadata);
        }
      }
      return $response;
    }
    catch (InvalidParameterException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new ResourceResponse($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_BAD_REQUEST);
    }
    catch (CouldNotRetrieveContentException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new ResourceResponse($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_NOT_FOUND);
    }
    catch (UnavailableDecoupledViewDisplayException $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new ResourceResponse($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_NOT_IMPLEMENTED);
    }
    catch (QueryException $exception) {
      // Failure on query execution or preparation.
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new ResourceResponse(Settings::get(RequestEntityInterface::SETTINGS__STATE__DEBUG_ENABLED) ? $this->t('There was an error in the query: @message.', ['@message' => $exception->getMessage()]) : NULL, Response::HTTP_BAD_REQUEST);
    }
    catch (\Exception $exception) {
      $this->getLogger('decoupled_toolbox')
        ->error($exception->getMessage());
      return new ResourceResponse($this->decoupledRequestEntity::exceptionToResponseMessage($exception), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

  }

}
