<?php

namespace Drupal\decoupled_toolbox\Plugin\rest\resource;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Controller\DecoupledControllerEvents;
use Drupal\decoupled_toolbox\Event\AlterQueryEvent;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\Event\ProcessableCheckEvent;
use Drupal\decoupled_toolbox\Service\DecoupledRendererInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for entity decoupled data.
 *
 * @todo What's the purpose of this class?
 */
abstract class CollectionResourceBase extends ResourceBase {

  use LoggerChannelTrait;

  protected const EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT = 'decoupled_toolbox.controller.rendered_output.built';

  protected const SETTINGS__STATE__DEBUG_ENABLED = 'decoupled_toolbox.state.debug_enabled';

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Decoupled renderer service.
   *
   * @var \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface
   */
  protected $decoupledRenderer;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Constructs a CollectionResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   Request object.
   * @param \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface $decoupledRenderer
   *   Decoupled renderer.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, Request $current_request, DecoupledRendererInterface $decoupledRenderer, EntityTypeBundleInfoInterface $entityTypeBundleInfo, EntityTypeManagerInterface $entityTypeManager, EventDispatcherInterface $eventDispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->decoupledRenderer = $decoupledRenderer;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('example_rest'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('decoupled.renderer'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * Gives a last opportunity to alter the query before execution.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to alter.
   */
  protected function alterQuery(QueryInterface $query): void {
    $this->eventDispatcher->dispatch(new AlterQueryEvent($query), DecoupledControllerEvents::EVENT__ALTER_QUERY);
  }

  /**
   * Additional checks if this controller must process the given request.
   *
   * This is used to let other modules perform additional checks, e.g.:
   *   - limit allowed parameters,
   *   - filter out unwanted requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object from controller.
   * @param string $type
   *   Entity type ID from controller.
   * @param string $bundle
   *   Bundle ID from controller.
   *
   * @return bool
   *   TRUE if processable, FALSE otherwise.
   */
  protected function checkIfProcessable(Request $request, $type, $bundle): bool {
    $processableCheckEvent = new ProcessableCheckEvent($request, $type, $bundle);
    $this->eventDispatcher->dispatch($processableCheckEvent, DecoupledControllerEvents::EVENT__PROCESSABLE_CHECK);

    if (empty($processableCheckEvent->getAccessResult())) {
      // No answer means processable (no event subscriber set this).
      return TRUE;
    }

    if ($processableCheckEvent->getAccessResult()->isForbidden()) {
      // Do not process it.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * This will check debug flags.
   *
   * @param \Exception $exception
   *   The exception to process.
   *
   * @return string|null
   *   The exception message, or NULL if debug mode is disabled.
   */
  public static function exceptionToResponseMessage(\Exception $exception): ?string {
    if (Settings::get(static::SETTINGS__STATE__DEBUG_ENABLED) === TRUE) {
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    // TODO: temp debug to check if settings.local.php is loaded.
    // TODO: this part must be removed later.
    $allSettings = Settings::getAll();
    if (!isset($allSettings[static::SETTINGS__STATE__DEBUG_ENABLED])) {
      // The setting is not set. Log it and temporarily return the message.
      \Drupal::logger('decoupled_toolbox')
        ->debug('Debug enabled setting not set.');
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    return NULL;
  }

  /**
   * Call this when the collection has gathered all items.
   *
   * @param array $renderedOutput
   *   Collection array, passed by reference for direct edit.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The base entity.
   * @param array $cacheTags
   *   Cache tags associated to the render.
   * @param string $display
   *   Display used for the render.
   */
  protected function onBaseEntityRenderBuilt(array &$renderedOutput, ContentEntityInterface $entity, array &$cacheTags, $display): void {
    // Let other handlers alter the output.
    $event = new OnRenderedOutputBuiltEvent($renderedOutput, $entity, $cacheTags, $display);
    $this->eventDispatcher->dispatch($event, static::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT);

    // Replace the output with the altered one.
    $renderedOutput = $event->getRenderedOutput();
  }

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  protected function validateQueryParameterAsPositiveInteger($parameter): bool {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) !== FALSE;
  }

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  protected function validateQueryParameterAsZeroAndPositiveInteger($parameter): bool {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]]) !== FALSE;
  }

}
