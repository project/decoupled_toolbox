<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\FieldValueAndOptions;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Standard field location solver.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class LocationSolver implements LocationSolverInterface, EventSubscriberInterface {

  /**
   * An instance of token manager service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  use LoggerChannelTrait;

  /**
   * Cache tags array for the processed field.
   *
   * @todo This is not appropriate to have this property here.
   *   This will work only with a single field. (This is a service, here.)
   *
   * @var array
   */
  private $processedFieldCacheTags;

  /**
   * {@inheritdoc}
   */
  public function __construct(Token $token) {
    $this->token = $token;
    $this->processedFieldCacheTags = [];
  }

  /**
   * Adds a cache tag associated to this field formatter.
   *
   * Call this when the field formatter has processed content which needs cache
   * tag for invalidation.
   *
   * @todo [#3484092] This is unused and not documented.
   *
   * @param array|string $tag
   *   The cache tag to add for the processed field.
   */
  protected function addCacheTag($tag) {
    if (is_array($tag)) {
      $this->processedFieldCacheTags = array_merge($this->processedFieldCacheTags, $tag);
      return TRUE;
    }
    $this->processedFieldCacheTags[] = $tag;
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @todo [#3484092] This is unused and not documented.
   */
  public function getacheTags() {

    if (empty($this->processedFieldCacheTags)) {
      // This may happen if the child formatter has not set any cache tags.
      return [];
    }

    return $this->processedFieldCacheTags;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RequestEntityInterface::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT] = ['onRenderedOutputBuilt'];
    return $events;
  }

  /**
   * Tells whether an array is sequential or not.
   *
   * This helps to not create useless indices in sequential arrays. Currently
   * not in use. Remove if really not needed.
   *
   * @todo [#3484092] Why is this method unused?
   *
   * @param array $array
   *   The array.
   *
   * @return bool
   *   TRUE if sequential, FALSE otherwise.
   */
  private function isSequentialArray(array $array) {
    return array_keys($array) === range(0, count($array) - 1);
  }

  /**
   * Reacts on rendered output built.
   *
   * @param \Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent $event
   *   Event object.
   */
  public function onRenderedOutputBuilt(OnRenderedOutputBuiltEvent $event) {
    $outputResult = [];
    $this->solve($event->getRenderedOutput(), $outputResult, $event->getEntity());

    // Replace the rendered output.
    $event->setRenderedOutput($outputResult);
  }

  /**
   * Recursive lookup for array values.
   *
   * @param array $outputSourceExtract
   *   Extract from the output source array on which we can work.
   * @param array $outputResult
   *   The final output result array on which we can work directly (useful for
   *   absolute positioning of values).
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   @todo [#3484092] What is this argument?
   *
   * @return array
   *   The results of this lookup (contains children).
   */
  private function recursiveLookupArray(array $outputSourceExtract, array &$outputResult, EntityInterface $entity) {
    $localDescendantsResult = [];

    // Read each item.
    foreach ($outputSourceExtract as $key => $sub) {
      if (is_array($sub)) {
        // Recurse further.
        $localDescendantsResult[$key] = $this->recursiveLookupArray($sub, $outputResult, $entity);
        continue;
      }
      if ($sub instanceof FieldValueAndOptions) {
        $fvoResult = $this->recursiveLookupFieldValueAndOptions($sub, $outputResult, $entity);

        if (is_null($fvoResult)) {
          // The field has been moved using absolute method.
          // No need to process further.
          continue;
        }

        // When building a (sub)array with FieldValueAndOptions, it likely means
        // we are building the root output of an entity, because it is in the
        // display form that we set properties in the formatters which generate
        // such objects to process. In other words, FieldValueAndOptions objects
        // and other value types should never be mixed up in a same array.
        // At this stage, the parent array has already been correctly ordered by
        // weight.
        // Thus it is safe to operate by setting directly the key.
        if (!empty($localDescendantsResult[$fvoResult['key']])) {
          if (!is_array($localDescendantsResult[$fvoResult['key']])) {
            $localDescendantsResult[$fvoResult['key']] = [$localDescendantsResult[$fvoResult['key']]];
          }
          if (!is_array($fvoResult['value'])) {
            $fvoResult['value'] = [$fvoResult['value']];
          }
          $localDescendantsResult[$fvoResult['key']] = array_merge(
            $localDescendantsResult[$fvoResult['key']],
            $fvoResult['value']);
          continue;
        }
        $localDescendantsResult[$fvoResult['key']] = $fvoResult['value'];
        continue;
      }

      // This is likely a value which contains scalar data.
      $localDescendantsResult[$key] = $sub;
    }

    return $localDescendantsResult;
  }

  /**
   * Call this when starting solving.
   *
   * @param mixed $outputSource
   *   The output source reference. Should not be modified. May be an array
   *   (expected usual case) or a FieldValueOptions object, or a scalar (really
   *   unexpected because it means the output is a single value not managed by
   *   FieldValueOptions container).
   * @param array $outputResult
   *   The output source result.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   @todo [#3484092] What is this argument?
   *
   * @return void|null
   *   NULL if process is done, void if the output source is updated.
   */
  private function recursiveLookupEntryPoint(&$outputSource, array &$outputResult, EntityInterface $entity) {
    if (is_array($outputSource)) {
      $localDescendantsResult = $this->recursiveLookupArray($outputSource, $outputResult, $entity);
    }
    elseif ($outputSource instanceof FieldValueAndOptions) {
      $localDescendantsResult = $this->recursiveLookupFieldValueAndOptions($outputSource, $outputResult, $entity);
    }
    else {
      // Unlikely to happen, but meh.
      $localDescendantsResult = $outputSource;
    }

    if (is_null($localDescendantsResult)) {
      // The field has been moved using absolute method.
      // No need to process further.
      return NULL;
    }

    // Finalize the merge with the $outputResult array.
    // NestedArray::mergeDeepArray(...) is absolutely necessary to correctly
    // merge the arrays as expected (+ operator and array_merge(...) do not
    // work).
    $outputResult = NestedArray::mergeDeepArray([
      $outputResult,
      $localDescendantsResult,
    ], TRUE);
  }

  /**
   * Recursive lookup for a single FieldValueAndOptions value.
   *
   * @param \Drupal\decoupled_toolbox\FieldValueAndOptions $outputSourceExtract
   *   Extract from the output source array on which we can work.
   * @param array $outputResult
   *   The final output result array on which we can work directly (useful for
   *   absolute positioning of values).
   *
   * @return array
   *   The results of this lookup (contains children).
   */
  private function recursiveLookupFieldValueAndOptions(FieldValueAndOptions $outputSourceExtract, array &$outputResult, EntityInterface $entity) {
    $itemKey = $outputSourceExtract->getDecoupledFieldKey();
    $itemValue = $outputSourceExtract->getValue();
    $itemLocation = $outputSourceExtract->getDecoupledFieldLocation();

    if (is_array($itemValue)) {
      $localDescendantsResult = $this->recursiveLookupArray($itemValue, $outputResult, $entity);
    }
    elseif ($itemValue instanceof FieldValueAndOptions) {
      $localDescendantsResult = $this->recursiveLookupFieldValueAndOptions($itemValue, $outputResult, $entity);
    }
    else {
      // Scalar values (integer, strings, etc).
      $localDescendantsResult = $itemValue;
    }

    if (!empty($itemLocation)) {
      return $this->buildLocation($outputResult, $itemLocation, $itemKey, $localDescendantsResult, $entity);
    }

    // Simply locate the value as usual.
    return [
      'key' => $itemKey,
      'value' => $localDescendantsResult,
    ];
  }

  /**
   * Rebuild outputResult depending path location.
   *
   * @param array $outputResult
   *   The final output result array on which we can work directly (useful for
   *   absolute positioning of values).
   * @param string $itemLocation
   *   The item path location.
   * @param string $itemKey
   *   The decoupled toolbox field key.
   * @param $localDescendantsResult
   *   @todo [#3484092] What is this argument?
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   @todo [#3484092] What is this argument?
   *
   * @return array|null
   *   @todo [#3484092] What is returned?
   */
  private function buildLocation(array &$outputResult, $itemLocation, $itemKey, $localDescendantsResult, EntityInterface $entity) {

    $bubbleable_metadata = new BubbleableMetadata();
    // A location has been defined.
    // Break the $itemLocation.
    $hierarchy = explode('/', $itemLocation);
    // Token replace on each element to avoid side effect on explode.
    foreach ($hierarchy as &$hier) {
      if (empty($hier)) {
        continue;
      }
      $hier = $this->token->replace($hier, [$entity->getEntityTypeId() => $entity], [], $bubbleable_metadata);
    }
    if (!empty($bubbleable_metadata->getCacheTags())) {
      $this->addCacheTag($bubbleable_metadata->getCacheTags());
    }
    $absolutePath = $hierarchy[0] === '';

    if ($absolutePath) {
      // Work directly on the finalResult array.
      // Reverse the hierarchy to reverse build the current array.
      $reverseHierarchy = array_reverse($hierarchy);

      // Remove the last item, which is an empty string (because it is an
      // absolute hierarchy).
      array_pop($reverseHierarchy);

      // Start by setting the value (which may be an array or a scalar).
      $array = [$itemKey => $localDescendantsResult];

      foreach ($reverseHierarchy as $step) {
        if ($step === '') {
          // Empty step means: "insert with no key nor delta, aka append".
          $array = [$array];
          continue;
        }

        $array = [$step => $array];
      }

      // Return NULL to tell caller that this item is already placed.
      // Finally merge the arrays.
      // NestedArray::mergeDeepArray() is necessary (array_merge(...) nor +
      // operator not working).
      $outputResult = array_merge_recursive(
        $outputResult,
        $array,
      );

      // Return NULL to tell caller that this item is already placed.
      return NULL;
    }
    $res = $localDescendantsResult;
    $reverseHierarchy = array_reverse($hierarchy);

    // Remove the last item, which is an empty string (because it is an
    // absolute hierarchy).
    $first = array_pop($reverseHierarchy);
    array_unshift($reverseHierarchy, $itemKey);
    foreach ($reverseHierarchy as $hierar) {
      $res = [$hierar => $res];
    }
    return ['key' => $first, 'value' => $res];
  }

  /**
   * {@inheritdoc}
   */
  public function solve(array &$outputSource, array &$outputResult, EntityInterface $entity) {
    // Solving must be done on a new array.
    $this->recursiveLookupEntryPoint($outputSource, $outputResult, $entity);
  }

}
