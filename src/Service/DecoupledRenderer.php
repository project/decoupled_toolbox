<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Event\RenderedOutputEvent;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Drupal\decoupled_toolbox\Exception\InvalidContentException;
use Drupal\decoupled_toolbox\Exception\InvalidFormatterSettingsException;
use Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException;
use Drupal\decoupled_toolbox\Exception\UnexpectedFormatterException;
use Drupal\decoupled_toolbox\Plugin\Field\FieldFormatter\DecoupledFormatterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Renders an entity for decoupled.
 */
class DecoupledRenderer implements DecoupledRendererInterface {

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * Array containing entities which are currently rendering.
   *
   * Use this to prevent entity reference infinite loops.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  private $currentEntityRenderStack = [];

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Contains a collection of EntityViewDisplayInterface for reuse.
   *
   * Items are indexed by ID such as "node.article.teaser".
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface[]
   */
  private $entityViewDisplayCache = [];

  /**
   * Entity view display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $entityViewDisplayStorage;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * A flag telling if we want to attempt to retrieve the entity from cache.
   *
   * Set to TRUE if we want to, FALSE otherwise.
   *
   * @var bool
   */
  private $useCachedEntity;

  use LoggerChannelTrait;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    CacheBackendInterface $cacheBackend,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher,
    LanguageManagerInterface $languageManager) {
    $this->cacheBackend = $cacheBackend;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->languageManager = $languageManager;
    $this->entityViewDisplayStorage = $entityTypeManager->getStorage('entity_view_display');

    $this->useCachedEntity = (bool) Settings::get(static::SETTINGS__CACHE__ENABLED, TRUE);
  }

  /**
   * Adds the specified entity to the current entity render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to add.
   * @param string $viewMode
   *   The entity view mode.
   */
  private function addEntityToCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode): void {
    $this->currentEntityRenderStack[$this->getEntityRenderStackEntityId($entity, $viewMode)] = $entity;
  }

  /**
   * Caches the render result.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which was rendered.
   * @param string $viewMode
   *   The entity view mode.
   * @param array $finalizedRender
   *   Array of values ready for decoupled output.
   * @param array $cacheTagsToMerge
   *   Array of additional cache tags to merge.
   */
  private function cacheRenderedEntityResult(ContentEntityInterface $entity, $viewMode, array $finalizedRender, array $cacheTagsToMerge): void {
    $mergedCacheTags = Cache::mergeTags($cacheTagsToMerge, $entity->getCacheTags());
    $cacheId = $this->getCacheIdForEntityAndViewMode($entity, $viewMode);
    $this->cacheBackend->set($cacheId, [
      'render' => $finalizedRender,
      'entity' => $entity,
    ], Cache::PERMANENT, $mergedCacheTags);
  }

  /**
   * Disables the cache system.
   */
  public function disableCache(): void {
    $this->useCachedEntity = FALSE;
  }

  /**
   * Enables the cache system.
   */
  public function enableCache(): void {
    $this->useCachedEntity = TRUE;
  }

  /**
   * Finalizes the fields to render.
   *
   * The output is freed from metadata and ready for serialization such as a
   * JSON object.
   *
   * @param array $fieldsToRender
   *   Fields to render as given by $this->renderEntity(...).
   *
   * @return array
   *   True values without metadata ready for serialization.
   */
  private function finalizeFieldsToRender(array $fieldsToRender): array {
    $finalized = [];

    foreach ($fieldsToRender as $item) {
      // $item['renderedFieldValue'] may be an associative array of a
      // FieldValueAndOptions object.
      $finalized[] = $item['renderedFieldValue'];
    }

    return $finalized;
  }

  /**
   * Gets the cache ID for the specified entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Cache ID ready for use.
   */
  private function getCacheIdForEntityAndViewMode(ContentEntityInterface $entity, $viewMode): string {
    return $this->getCacheIdForEntityTypeAndIdAndViewMode($entity->getEntityTypeId(), $entity->id(), $viewMode);
  }

  /**
   * Gets the cache ID for the specified entity.
   *
   * @param string $entityTypeId
   *   Entity type ID.
   * @param int $id
   *   ID of the entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Cache ID ready for use.
   */
  private function getCacheIdForEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode): string {
    return 'decoupled.' . $entityTypeId . '.' . $id . '.' . $viewMode;
  }

  /**
   * Retrieves the rendered entity cached data.
   *
   * @param string $entityTypeId
   *   Entity type ID.
   * @param int $id
   *   ID of the entity.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return object|null
   *   Cached data object, or NULL if not yet cached.
   *   This is a stdClass object containing cache values.
   */
  private function getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode): ?object {
    $cacheId = $this->getCacheIdForEntityTypeAndIdAndViewMode($entityTypeId, $id, $viewMode);

    $cacheObject = $this->cacheBackend->get($cacheId);

    if (empty($cacheObject)) {
      return NULL;
    }

    return $cacheObject;
  }

  /**
   * Gets the ID used for the currentEntityRenderStack array.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity which we want to get the entity render stack ID.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return string
   *   Entity render stack ID.
   */
  private function getEntityRenderStackEntityId(ContentEntityInterface $entity, $viewMode): string {
    return $entity->getEntityTypeId() . '.' . $entity->id() . '.' . $viewMode;
  }

  /**
   * Gets the entity view display for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the view display.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface|null
   *   The entity view display object, or NULL if not found.
   */
  private function getViewDisplayForEntity(ContentEntityInterface $entity, $viewMode) {
    $entityTypeId = $entity->getEntityTypeId();
    $entityBundle = $entity->bundle();

    $viewDisplayId = $entityTypeId . '.' . $entityBundle . '.' . $viewMode;

    if (isset($this->entityViewDisplayCache[$viewDisplayId])) {
      // Already loaded. Reuse the value. May return NULL if the entity view
      // display does not exist.
      return $this->entityViewDisplayCache[$viewDisplayId];
    }

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $viewDisplay */
    $viewDisplay = $this->entityViewDisplayStorage->load($viewDisplayId);

    // Cache the result.
    $this->entityViewDisplayCache[$viewDisplayId] = $viewDisplay;

    return $viewDisplay;
  }

  /**
   * Tells whether the given entity is already in the current render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   * @param string $viewMode
   *   The entity view mode.
   *
   * @return bool
   *   TRUE if already in current render stack. FALSE otherwise.
   */
  private function isEntityInCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode): bool {
    return array_key_exists($this->getEntityRenderStackEntityId($entity, $viewMode), $this->currentEntityRenderStack);
  }

  /**
   * Tells if the cache is being used or not.
   *
   * @return bool
   *   The cache usage.
   */
  public function isUsingCache(): bool {
    return $this->useCachedEntity;
  }

  /**
   * Adds the specified entity to the current entity render stack.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to remove.
   * @param string $viewMode
   *   The entity view mode.
   */
  private function removeEntityFromCurrentEntityRenderStack(ContentEntityInterface $entity, $viewMode): void {
    unset($this->currentEntityRenderStack[$this->getEntityRenderStackEntityId($entity, $viewMode)]);
  }

  /**
   * {@inheritdoc}
   */
  public function renderByEntityTypeAndId($entityTypeId, $id, array &$cacheTags, $display = EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID): array {
    // Use the default view mode.
    if ($this->useCachedEntity) {
      // Attempt to load the entity from cache.
      $cachedDataObject = $this->getEntityCachedDataByEntityTypeAndIdAndViewMode($entityTypeId, $id, $display);

      if (!empty($cachedDataObject)) {
        // Entry found in cache.
        $cacheTags = $cachedDataObject->tags;

        // The returned value can be an empty array and be a valid value.
        return $cachedDataObject->data;
      }
    }

    try {
      $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    }
    catch (\Exception $exception) {
      $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
      // Storage error.
      throw new CouldNotRetrieveContentException();
    }

    $entity = $entityStorage->load($id);

    if (empty($entity)) {
      throw new CouldNotRetrieveContentException();
    }

    if (!$entity instanceof ContentEntityInterface) {
      throw new InvalidContentException();
    }

    $collectedCacheableMetadata = CacheableMetadata::createFromRenderArray([]);

    // Render the entity. This will (maybe) put the entity
    // in cache upon completion.
    $rendered = $this->renderEntity($entity, $display, $collectedCacheableMetadata);

    $cacheTags = $collectedCacheableMetadata->getCacheTags();

    return ['render' => $rendered, 'entity' => $entity];
  }

  /**
   * Renders the specified content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to render.
   * @param string $viewMode
   *   View mode to use. Defaults to
   *   EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $collectedCacheableMetadata
   *   Will be populated with collected cacheable metadata
   *   for the rendered entity.
   *
   * @return array
   *   The rendered content as array, ready for serialization.
   *
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   The entity has no decoupled view display set.
   */
  private function renderEntity(
    ContentEntityInterface $entity,
    string $viewMode = EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID,
    ?CacheableMetadata $collectedCacheableMetadata = NULL
  ): array {
    if ($this->isEntityInCurrentEntityRenderStack($entity, $viewMode)) {
      // The entity is already rendering in the stack. We have to prevent
      // infinite loops by not attempting to render this entity again.
      return [
        $entity->getEntityType()->getKey('id') => $entity->id(),
        'infiniteInclusionPrevention' => TRUE,
      ];
    }

    $this->addEntityToCurrentEntityRenderStack($entity, $viewMode);

    try {
      // Get the decoupled view mode for the given entity.
      $viewDisplay = $this->getViewDisplayForEntity($entity, $viewMode);

      if (empty($viewDisplay)) {
        throw new UnavailableDecoupledViewDisplayException();
      }

      // Get active fields on this view display.
      $enabledFields = $viewDisplay->getComponents();

      // Get the fields and their weight.
      $langCode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
        ->getId();

      $fieldsToRender = [];
      $cacheTagsFromFields = $viewDisplay->getCacheTags();
      $collectedCacheableMetadata->merge(CacheableMetadata::createFromObject($viewDisplay));

      foreach ($enabledFields as $fieldId => $fieldSettings) {
        $fieldRenderer = $viewDisplay->getRenderer($fieldId);

        if (!$fieldRenderer instanceof DecoupledFormatterInterface) {
          // Filter out fields without a valid formatter.
          continue;
        }

        if ($fieldRenderer->getSetting('decoupled_field_hide_empty') && $entity->get($fieldId)->isEmpty()) {
          continue;
        }

        $fieldItemList = $entity->get($fieldId);

        try {
          $renderedFieldValue = $fieldRenderer->viewElementsForDecoupled($fieldItemList, $langCode);

          // Retrieve cache tags from the processed field. This is useful for
          // entity references or specific formatters which may need to
          // invalidate caches.
          $cacheTagsFromFields = Cache::mergeTags($cacheTagsFromFields, $renderedFieldValue->getCacheableMetadata()?->getCacheTags() ?? []);

          $collectedCacheableMetadata && $renderedFieldValue->getCacheableMetadata() && $collectedCacheableMetadata->merge($renderedFieldValue->getCacheableMetadata());
        }
        catch (InvalidContentException $exception) {
          $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
          // Do not block on content error.
          // Silently ignore this field.
          continue;
        }
        catch (InvalidFormatterSettingsException $exception) {
          $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
          // Reporting the error in watchdog may result in database spam.
          // Silently ignore this field.
          continue;
        }
        catch (UnexpectedFormatterException $exception) {
          $this->getLogger('decoupled_toolbox')->error($exception->getMessage());
          // A formatter was set on a field which does not support it. E.g. an
          // entity formatter was set on a field which does not implements
          // EntityReferenceFieldItemListInterface.
          // Silently ignore this field.
          continue;
        }

        $fieldsToRender[] = [
          'renderedFieldValue' => $renderedFieldValue,
          'weight' => $fieldSettings['weight'],
        ];
      }

      // Respect the weight set on the field UI of the view mode.
      $this->sortFieldsToRenderByWeight($fieldsToRender);

      // Remove transitive metadata (which serve only for the first stage of
      // the output building (static representation building)).
      $finalValues = $this->finalizeFieldsToRender($fieldsToRender);

      // Let other handlers alter the output.
      $event = new RenderedOutputEvent($entity, $finalValues, $cacheTagsFromFields);
      $this->eventDispatcher->dispatch($event, static::EVENT__RENDERER__OUTPUT__RENDERED__PREFIX . $entity->getEntityTypeId());

      // Cache the final values using the fields' cache metadata.
      $this->shouldUseCache() && $this->cacheRenderedEntityResult($entity, $viewMode, $finalValues, $cacheTagsFromFields);
    }
    finally {
      // Remove the processed entity from render stack, even if
      // the process failed.
      // We are safe from infinite loops.
      $this->removeEntityFromCurrentEntityRenderStack($entity, $viewMode);
    }

    return $finalValues;
  }

  /**
   * Tells whether we should cache or retrieve from cache, or not.
   *
   * @return bool
   *   TRUE if cache should be used.
   */
  private function shouldUseCache(): bool {
    return $this->useCachedEntity;
  }

  /**
   * Sorts the fields to render by their weight key.
   *
   * @param array $fieldsToRender
   *   Array of fields to render.
   */
  private function sortFieldsToRenderByWeight(array &$fieldsToRender): void {
    usort($fieldsToRender, function ($a, $b) {
      return $a['weight'] <=> $b['weight'];
    });
  }

}
