<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Site\Settings;
use Drupal\decoupled_toolbox\Controller\DecoupledControllerEvents;
use Drupal\decoupled_toolbox\Controller\FilterTrait;
use Drupal\decoupled_toolbox\Event\AlterQueryEvent;
use Drupal\decoupled_toolbox\Event\OnRenderedOutputBuiltEvent;
use Drupal\decoupled_toolbox\Event\ProcessableCheckEvent;
use Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Class RequestEntity get collection by a request.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class RequestEntity implements RequestEntityInterface {

  use FilterTrait;

  /**
   * Decoupled renderer service.
   *
   * @var \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface
   */
  protected $decoupledRenderer;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The offset of query.
   *
   * @var int|null
   */
  private $offset;

  /**
   * The limit of query.
   *
   * @var int|null
   */
  private $limit;

  /**
   * EntityDecoupledDataController constructor.
   *
   * @param \Drupal\decoupled_toolbox\Service\DecoupledRendererInterface $decoupledRenderer
   *   The decoupled renderer service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   Entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   */
  public function __construct(
    DecoupledRendererInterface $decoupledRenderer,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher,
    ConfigFactoryInterface $config_factory) {
    $this->decoupledRenderer = $decoupledRenderer;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->configFactory = $config_factory;
    $this->offset = $this->limit = NULL;
  }

  /**
   * Validate query and parameter of request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request url.
   * @param string $type
   *   The entity type url parameter.
   * @param string $bundle
   *   The bundle url  parameter.
   */
  public function validateParameterAndOption(Request $request, $type, $bundle) {
    $offset = $request->query->get('offset');
    if (empty($offset)) {
      $this->offset = NULL;
    }
    elseif (!self::validateQueryParameterAsZeroAndPositiveInteger($offset)) {
      throw new InvalidParameterException($this->t('Invalid offset parameter.'));
    }

    $this->limit = $request->query->get('limit');

    if (empty($this->limit)) {
      // Limit not send by user or equals 0.
      // Get the default settings.
      $decoupled_settings = $this->configFactory->get('decoupled_toolbox.settings');
      $limit_settings = $decoupled_settings->get('limit');
      $limit_settings = is_array($limit_settings) ? $limit_settings : [];
      $limit_settings = array_merge([
        'value' => NULL,
        'unlimited' => FALSE,
        'required' => FALSE,
      ], $limit_settings);

      if (($limit_settings['unlimited'] ?? FALSE) == TRUE) {
        // The 'unlimited' setting takes precedence over the 'value' one.
        $this->limit = NULL;
      }
      elseif (($limit_settings['required'] ?? FALSE) == TRUE) {
        throw new InvalidParameterException($this->t('Limit parameter is required.'));
      }
      else {
        // Use the default value.
        $this->limit = $limit_settings['value'] ?? NULL;
      }
    }
    elseif (!self::validateQueryParameterAsPositiveInteger($this->limit)) {
      throw new InvalidParameterException($this->t('Invalid limit parameter.'));
    }

    // Make sure the entity type and bundle exist.
    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($type);

    if (empty($bundleInfo[$bundle])) {
      throw new InvalidParameterException($this->t('Invalid entity type or bundle.'));
    }

    if (!$this->checkIfProcessable($request, $type, $bundle)) {
      // The requested data is not allowed.
      throw new InvalidParameterException($this->t('Not allowed to process the requested data.'));
    }
  }

  /**
   * The offset of query.
   *
   * @return int|null
   *   Offset.
   */
  public function getOffset() {
    return $this->offset;
  }

  /**
   * The limit of query.
   *
   * @return int|null
   *   Limit.
   */
  public function getLimit() {
    return $this->limit;
  }

  /**
   * Gets a collection of entity decoupled data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Symfony request object.
   * @param string $type
   *   Entity type ID.
   * @param string $bundle
   *   Bundle of the entities to work on.
   * @param array $options
   *   Options to control the result. Keys:
   *   - "accessCheck", bool: Tells whether the entity query access check must
   *     occur or not. When unset, we will not perform access check (= FALSE).
   *
   * @return array
   *   An array of render result and cache tags.
   *
   * @throws \Drupal\Core\Entity\Query\QueryException
   *   Error on the query execution or preparation.
   * @throws \Drupal\decoupled_toolbox\Exception\CouldNotRetrieveContentException
   *   Content not found or storage error.
   * @throws \Drupal\decoupled_toolbox\Exception\InvalidContentException
   *   Not a valid content entity.
   * @throws \Drupal\decoupled_toolbox\Exception\UnavailableDecoupledViewDisplayException
   *   Entity type is not suitable for decoupled rendering.
   */
  public function getCollection(Request $request, $type, $bundle, ?array $options = NULL) {
    $this->validateParameterAndOption($request, $type, $bundle);

    // If bundle info was found, we assume the entity type exists.
    try {
      $entityStorage = $this->entityTypeManager->getStorage($type);
    }
    catch (\Exception $exception) {
      // Something really wrong with the database occurred because valid
      // entities must have a storage. Do not process anymore.
      throw new CouldNotRetrieveContentException();
    }

    // Prepare the query.
    $query = $entityStorage->getQuery()
      // By default, we do not perform an accessCheck.
      // This may change in the future.
      ->accessCheck($options['accessCheck'] ?? FALSE)
      ->range($this->offset, $this->limit);

    // Get the bundle key if it exists.
    try {
      $entityTypeDefinition = $this->entityTypeManager->getDefinition($type);
    }
    catch (PluginNotFoundException $exception) {
      throw new InvalidParameterException($this->t('Invalid entity type.'));
    }

    $bundleKey = $entityTypeDefinition->getKey('bundle');

    if (!empty($bundleKey)) {
      // Not a bundleless entity.
      $query->condition($bundleKey, $bundle);
    }

    $this->appendConditionsFromQueryParameters($request, $query, $type, $bundle);
    $this->alterQuery($query);

    // May fail with a QueryException.
    $ids = $query->execute();

    $display = $request->query->get('display') ?? EntityViewDisplayManagerInterface::ENTITY_DECOUPLED_VIEW_MODE_ID;
    $this->decoupledRenderer->disableCache();
    $cacheTags = $rendered = [];
    // Add tags config.
    $cacheTags[] = 'config:decoupled_toolbox.settings';
    foreach ($ids as $id) {
      // Use $cacheTags parameter if the response needs to be cached.
      $fullRenderedEntity = $this->decoupledRenderer->renderByEntityTypeAndId($type, $id, $cacheTags, $display);

      // Let other modules / events perform last changes.
      $this->onBaseEntityRenderBuilt($fullRenderedEntity['render'], $fullRenderedEntity['entity'], $cacheTags, $display);
      if ($this->configFactory->get('decoupled_toolbox.settings')
        ->get('include_version')) {
        $fullRenderedEntity['render']['decoupled_toolbox'] = $this->configFactory->get('decoupled_toolbox.settings')
          ->get('version');
      }
      $rendered[] = $fullRenderedEntity['render'];
    }

    return ['render' => $rendered, 'cache_tags' => $cacheTags];

  }

  /**
   * {@inheritdoc}
   */
  public function alterQuery(QueryInterface $query) {
    $this->eventDispatcher->dispatch(new AlterQueryEvent($query), DecoupledControllerEvents::EVENT__ALTER_QUERY);
  }

  /**
   * {@inheritdoc}
   */
  public function checkIfProcessable(Request $request, $type, $bundle) {
    $processableCheckEvent = new ProcessableCheckEvent($request, $type, $bundle);
    $this->eventDispatcher->dispatch($processableCheckEvent, DecoupledControllerEvents::EVENT__PROCESSABLE_CHECK);

    if (empty($processableCheckEvent->getAccessResult())) {
      // No answer means processable (no event subscriber set this).
      return TRUE;
    }

    if ($processableCheckEvent->getAccessResult()->isForbidden()) {
      // Do not process it.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * This will check debug flags.
   *
   * @param \Exception $exception
   *   The exception to process.
   *
   * @return string|null
   *   The exception message, or NULL if debug mode is disabled.
   */
  public static function exceptionToResponseMessage(\Exception $exception) {
    if (Settings::get(static::SETTINGS__STATE__DEBUG_ENABLED) === TRUE) {
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    // TODO: temp debug to check if settings.local.php is loaded.
    // TODO: this part must be removed later.
    $allSettings = Settings::getAll();
    if (!isset($allSettings[static::SETTINGS__STATE__DEBUG_ENABLED])) {
      // The setting is not set. Log it and temporarily return the message.
      \Drupal::logger('decoupled_toolbox')
        ->debug('Debug enabled setting not set.');
      return $exception->getMessage() . "\n" . $exception->getTraceAsString();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function onBaseEntityRenderBuilt(array &$renderedOutput, ContentEntityInterface $entity, array &$cacheTags, $display) {
    // Let other handlers alter the output.
    $event = new OnRenderedOutputBuiltEvent($renderedOutput, $entity, $cacheTags, $display);
    $this->eventDispatcher->dispatch($event, static::EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT);

    // Replace the output with the altered one.
    $renderedOutput = $event->getRenderedOutput();
  }

  /**
   * {@inheritdoc}
   */
  public static function validateQueryParameterAsPositiveInteger($parameter) {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateQueryParameterAsZeroAndPositiveInteger($parameter) {
    return filter_var($parameter, FILTER_VALIDATE_INT, ['options' => ['min_range' => 0]]) !== FALSE;
  }

}
