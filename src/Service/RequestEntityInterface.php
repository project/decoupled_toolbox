<?php

namespace Drupal\decoupled_toolbox\Service;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Renders for decoupled.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
interface RequestEntityInterface {

  const EVENT__CONTROLLER__ON_RENDERED_OUTPUT_BUILT = 'decoupled_toolbox.controller.rendered_output.built';

  const SETTINGS__STATE__DEBUG_ENABLED = 'decoupled_toolbox.state.debug_enabled';

  /**
   * Additional checks if this controller must process the given request.
   *
   * This is used to let other modules perform additional checks, e.g.:
   *   - limit allowed parameters,
   *   - filter out unwanted requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object from controller.
   * @param string $type
   *   Entity type ID from controller.
   * @param string $bundle
   *   Bundle ID from controller.
   *
   * @return bool
   *   TRUE if processable, FALSE otherwise.
   */
  public function checkIfProcessable(Request $request, $type, $bundle);

  /**
   * Gives a last opportunity to alter the query before execution.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query to alter.
   */
  public function alterQuery(QueryInterface $query);

  /**
   * Call this when the collection has gathered all items.
   *
   * @param array $renderedOutput
   *   Collection array, passed by reference for direct edit.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Base entity.
   * @param array $cacheTags
   *   Cache tags.
   * @param string $display
   *   Display used for this render.
   */
  public function onBaseEntityRenderBuilt(array &$renderedOutput, ContentEntityInterface $entity, array &$cacheTags, $display);

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  public static function validateQueryParameterAsPositiveInteger($parameter);

  /**
   * Validates query parameters as ID such as entity IDs.
   *
   * @param mixed $parameter
   *   Parameter to test.
   *
   * @return bool
   *   TRUE if valid ID, FALSE otherwise.
   */
  public static function validateQueryParameterAsZeroAndPositiveInteger($parameter);

}
