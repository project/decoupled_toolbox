<?php

namespace Drupal\decoupled_toolbox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form.
 */
class DecoupledToolboxSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'decoupled_toolbox_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'decoupled_toolbox.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('decoupled_toolbox.settings');

    $form['ignore_missing_entity_reference'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore missing entity references'),
      '#default_value' => $config->get('ignore_missing_entity_reference') ?? FALSE,
      '#description' => $this->t('If checked, missing entity references will simply not be included in the decoupled output of entity reference fields, instead of triggering an exception. This happens when an entity has been deleted, but the references to it are still present in the database.'),
    ];

    $form['version'] = [
      '#type' => 'number',
      '#min' => 1.00,
      '#step' => 0.1,
      '#title' => $this->t('Version'),
      '#default_value' => $config->get('version') ?? 1.0,
      '#description' => $this->t('Version of API. The version increments automatically when you change your display.'),
    ];

    $form['include_version'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include version'),
      '#default_value' => $config->get('include_version') ?? TRUE,
      '#description' => $this->t('If checked, this version will be output in json.'),
    ];

    $limit = $config->get('limit') ?? [];
    $form['limit'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#open' => TRUE,
      '#title' => $this->t('Limit'),
      '#description' => $this->t('Change the behavior of decoupled API query limit.'),
    ];

    $form['limit']['unlimited'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Unlimited'),
      '#default_value' => $limit['unlimited'] ?? TRUE,
      '#description' => $this->t('Uncheck if you want to set your own limit as default limit value'),
    ];

    $form['limit']['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#default_value' => $limit['required'] ?? FALSE,
      '#description' => $this->t('Check if you want to set limit as required query parameter.'),
      '#states' => [
        'visible' => [
          'input[name="limit[unlimited]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['limit']['value'] = [
      '#type' => 'number',
      '#min' => 1,
      '#step' => 1,
      '#title' => $this->t('Default limit value'),
      '#default_value' => $limit['value'] ?? 1,
      '#description' => $this->t('The default limit value'),
      '#states' => [
        'visible' => [
          'input[name="limit[unlimited]"]' => ['checked' => FALSE],
          'input[name="limit[required]"]' => ['checked' => FALSE],
        ],
        'required' => [
          'input[name="limit[unlimited]"]' => ['checked' => FALSE],
          'input[name="limit[required]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $limit = $form_state->getValue('limit');
    $limit['unlimited'] = isset($limit['unlimited']) ? (boolean) $limit['unlimited'] : FALSE;
    $limit['required'] = isset($limit['required']) ? (boolean) $limit['required'] : FALSE;
    $limit['value'] = isset($limit['value']) ? (integer) $limit['value'] : 0;
    $this->configFactory->getEditable('decoupled_toolbox.settings')
      ->set('ignore_missing_entity_reference', (boolean) $form_state->getValue('ignore_missing_entity_reference'))
      ->set('version', (float) $form_state->getValue('version'))
      ->set('include_version', (boolean) $form_state->getValue('include_version'))
      ->set('limit', $form_state->getValue('limit'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
