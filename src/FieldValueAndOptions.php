<?php

namespace Drupal\decoupled_toolbox;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Represents a field value and its options.
 *
 * This is used when a decoupled field formatter has to bind field values and
 * options (such as location) in a way that real data and meta data (those
 * options) do not mix.
 *
 * @SuppressWarnings(PHPMD.CamelCaseParameterName)
 * @SuppressWarnings(PHPMD.CamelCaseVariableName)
 */
class FieldValueAndOptions {

  /**
   * Cacheable metadata associated to this field value.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata|null
   */
  private ?CacheableMetadata $cacheableMetadata;

  /**
   * Decoupled field key.
   *
   * @var string
   */
  private $decoupledFieldKey;

  /**
   * Options array.
   *
   * @var array
   */
  private $decoupledFieldLocation;

  /**
   * The real value of the field.
   *
   * @var mixed
   */
  private $value;

  /**
   * FieldValueAndOptions constructor.
   *
   * @param string $decoupledFieldKey
   *   The decoupled field key.
   * @param mixed $value
   *   The real value of the field.
   * @param string $decoupledFieldLocation
   *   The field location override, if any.
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   *   The bubbleable cacheable metadata of this value.
   *   Leave empty if not needed.
   */
  public function __construct($decoupledFieldKey, $value, $decoupledFieldLocation, ?CacheableMetadata $cacheableMetadata = NULL) {
    $this->decoupledFieldKey = $decoupledFieldKey;
    $this->decoupledFieldLocation = $decoupledFieldLocation;
    $this->value = $value;
    $this->cacheableMetadata = $cacheableMetadata;
  }

  /**
   * Gets the cacheable metadata of this value.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata|null
   *   The metadata, if any. NULL if no metadata.
   */
  public function getCacheableMetadata(): ?CacheableMetadata {
    return $this->cacheableMetadata;
  }

  /**
   * The decoupled field key.
   *
   * @return string
   *   Field key.
   */
  public function getDecoupledFieldKey() {
    return $this->decoupledFieldKey;
  }

  /**
   * The field decoupled location. May be empty if no override is needed.
   *
   * @return mixed
   *   Field decoupled location.
   */
  public function getDecoupledFieldLocation() {
    return $this->decoupledFieldLocation;
  }

  /**
   * The field value.
   *
   * @return mixed
   *   Field value.
   */
  public function getValue() {
    return $this->value;
  }

}
